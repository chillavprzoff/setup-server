#!/bin/bash

source ./scripts/general-setup.sh

sed -e "s/USER/$USER_ANSIBLE/" ./passwordless.yaml > ./yamls/passwordless.yaml

source ./scripts/add-sudo-pass.sh

source ./scripts/add-more-servers.sh

while true; do
  ansible-playbook -i ./inventory.yaml ./yamls/passwordless.yaml
  
  read -p 'retry? (Y/n): ' RETRY
  if [[ $RETRY == n ]]; then
    break
  fi
done
