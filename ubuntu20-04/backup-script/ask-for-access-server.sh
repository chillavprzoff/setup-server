#!/bin/bash

read -p 'Use password to connect to server? (y/n): ' SSH_PASSWORD_CONFIRM
if [[ $SSH_PASSWORD_CONFIRM == "y" ]]; then
  read -p 'SSH Password: ' SSH_PASSWORD
else
  read -p 'Path to Private Key: ' PRIVATEKEY
fi
if [[ $SSH_PASSWORD != "" ]]; then
  echo "    ansible_password: '$SSH_PASSWORD'" >> ./$INVENTORY_FILE 
  echo "    ansible_become_password: '$SSH_PASSWORD'" >> ./$INVENTORY_FILE 
fi
