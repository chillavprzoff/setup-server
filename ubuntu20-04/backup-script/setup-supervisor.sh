#!/bin/bash

if [[ $IPBASTION == "" ]]; then
  read -p 'Bastion IP: ' IPBASTION
fi
if [[ $SSHPORT == "" ]]; then
  read -p 'Bastion SSH Port: ' SSHPORT
fi
if [[ $IPSERVER == "" ]]; then
  read -p 'Server IP: ' IPSERVER
fi

read -p 'User (devops): ' USER_ANSIBLE

if [[ $USER_ANSIBLE == "" ]]; then
  USER_ANSIBLE="devops"
fi

if [[ $SSHPORT == "" ]]; then
  SSHPORT=22
fi

sed -e "s/SSHPORT/$SSHPORT/" -e "s/USER/$USER_ANSIBLE/" -e "s/IPSERVER/$IPSERVER/" -e "s/IPBASTION/$IPBASTION/" ./inventory.yaml-example > ./inventory.yaml

while true; do
  ansible-playbook -i ./inventory.yaml ./supervisor.yaml
  
  read -p 'retry? (Y/n): ' RETRY
  if [[ $RETRY == n ]]; then
    break
  fi
done
