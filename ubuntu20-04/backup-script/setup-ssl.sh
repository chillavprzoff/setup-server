#!/bin/bash

INVENTORY_FILE="inventory.yaml"
PLAYBOOK_FILE="ssl.yaml"

source ./scripts/general-setup.sh
source ./scripts/add-ssh-port.sh

read -p "Domain Name: " DOMAIN_NAME
echo "    DOMAIN_NAME: '$DOMAIN_NAME'" >> ./$INVENTORY_FILE
read -p "Your Email: " EMAIL
echo "    EMAIL: '$EMAIL'" >> ./$INVENTORY_FILE

while true; do
  ansible-playbook -i ./$INVENTORY_FILE ./$PLAYBOOK_FILE
  
  echo ""
  echo "----------------------------------------------------"
  echo "Installed SSL URL: https://$DOMAIN_NAME"
  echo "----------------------------------------------------"
  echo ""

  read -p 'retry? (Y/n): ' RETRY
  if [[ $RETRY == n ]]; then
    break
  fi
done
