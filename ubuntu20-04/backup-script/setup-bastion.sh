#!/bin/bash
function setupBastion() {

read -p 'App Name : ' APP_NAME
read -p 'Bastion IP: ' IPBASTION
read -p 'Path to Bastion Private Key: ' BASTIONKEY
read -p 'User Bastion: (default=ec2-user): ' USER_BASTION
if [[ $USER_BASTION == "" ]]; then
  USER_BASTION="ec2-user"
fi

chmod 600 $BASTIONKEY

read -p 'Path to your Public Key (default=~/.ssh/id_rsa.pub): ' PUBKEY

PUBKEY=$(<~/.ssh/id_rsa.pub)

if [[ $PUBKEY == "" ]]; then
  echo "Cannot read public key, plese make sure the path is correct!"
  echo "Exiting.."
  exit
fi

read -p 'Do you want to change ssh port? (port/N): ' SSHPORT

echo "[bastion]
$IPBASTION

[bastion:vars]
ansible_user=$USER_BASTION
ansible_ssh_private_key_file=$BASTIONKEY" > ./hosts/bastion

echo "APP_NAME: "${APP_NAME}"
PUBLIC_KEY: "$PUBKEY" 
SSHPORT: $SSHPORT" > ./envs/bastion.yaml

while true; do
  if [[ $SSHPORT != "" ]] || [[ $SSHPORT != "n" ]] || [[ $SSHPORT != "N" ]]; then
    ansible-playbook -i ./hosts/bastion ./bastion-change-port.yaml
  else
    ansible-playbook -i ./hosts/bastion ./bastion.yaml
  fi

  read -p "Create entry in ssh config for the bastion host? (Y/n): " SSH_CONFIG
  echo "" >> ~/.ssh/config
  if [[ $SSH_CONFIG != "n" ]]; then
    if [[ $SSHPORT != "" ]] || [[ $SSHPORT != "n" ]] || [[ $SSHPORT != "N" ]]; then
    echo "Host $APP_NAME.host
  Hostname $IPBASTION
  Port $SSHPORT
  User $(if [[ $USER_BASTION != "" ]]; then echo $USER_BASTION; else echo 'ec2-user'; fi)" >> ~/.ssh/config
    else
    echo "Host $APP_NAME.host
  Hostname $IPBASTION 
  Port 22
  User $(if [[ $USER_BASTION != "" ]]; then echo $USER_BASTION; else echo 'ec2-user'; fi)" >> ~/.ssh/config

    fi

    echo ""
    echo "------------------"
    echo "ssh $APP_NAME.host"
    echo "------------------"
    echo ""

  else
    if [[ $USER_BASTION != "" ]]; then
      echo "ssh $USER_BASTION@$IPBASTION $(if [[ $SSHPORT != "" ]]; then echo "'-p $SSHPORT'"; else echo ''; fi)";
    else
      echo "ssh ec2-user@$IPBASTION $(if [[ $SSHPORT != "" ]]; then echo "'-p $SSHPORT'"; else echo ''; fi)";
    fi

  fi
  
  read -p 'retry? (Y/n): ' RETRY
  if [[ $RETRY == n ]]; then
    break
  fi

done

}
