#!/bin/bash
read -p "Create entry in ssh config for the host? (Y/n): " SSH_CONFIG

if [[ $APP_NAME == "" ]]; then
  read -p 'App Name : ' APP_NAME
fi

echo "" >> ~/.ssh/config
if [[ $SSH_CONFIG != "n" ]]; then
  if [[ $SSHPORT != "" ]] || [[ $SSHPORT != "n" ]] || [[ $SSHPORT != "N" ]]; then
  echo "Host $APP_NAME
  Hostname $IPSERVER
  Port $SSHPORT
  User $(if [[ $USER_ANSIBLE != "" ]]; then echo $USER_ANSIBLE; else echo 'ec2-user'; fi)" >> ~/.ssh/config
  else
  echo "Host $APP_NAME
  Hostname $IPSERVER 
  Port 22
  User $(if [[ $USER_ANSIBLE != "" ]]; then echo $USER_ANSIBLE; else echo 'ec2-user'; fi)" >> ~/.ssh/config

  fi

  echo ""
  echo "------------------"
  echo "ssh $APP_NAME.host"
  echo "------------------"
  echo ""

else
  if [[ $USER_ANSIBLE != "" ]]; then
    echo "ssh $USER_ANSIBLE@$IPSERVER $(if [[ $SSHPORT != "" ]]; then echo "'-p $SSHPORT'"; else echo ''; fi)";
  else
    echo "ssh ec2-user@$IPSERVER $(if [[ $SSHPORT != "" ]]; then echo "'-p $SSHPORT'"; else echo ''; fi)";
  fi

fi
