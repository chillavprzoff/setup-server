#!/bin/bash

INVENTORY_FILE="inventory.yaml"
PLAYBOOK_FILE="laravel-setup-cli.yaml"

source ./scripts/general-setup.sh
source ./scripts/add-ssh-port.sh
source ./scripts/add-more-servers.sh

while true; do
  ansible-playbook -i ./$INVENTORY_FILE ./$PLAYBOOK_FILE
  
  read -p 'retry? (Y/n): ' RETRY
  if [[ $RETRY == "n" ]]; then
      
      
      read -p "Are you want to ssh the server? (Y/n) : " SSH_SERVER
      if [[ $SSH_SERVER != "n" ]]; then
        if [[ $SSHPORT != "" ]]; then
          ssh $USER_ANSIBLE@$IPSERVER -p $SSHPORT
        else
          ssh $USER_ANSIBLE@$IPSERVER 
        fi
      else
        break
      fi

    read -p 'exit from loop? (Y/n): ' LOOP
    if [[ $LOOP != "n" ]]; then
      break
    fi
  fi

done
