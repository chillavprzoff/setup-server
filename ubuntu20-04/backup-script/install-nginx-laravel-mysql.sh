#!/bin/bash
#variable
domain_name=$1
check_domain (){
        if  [ -z $domain_name ]
        then 
                echo "YOU DIDN'T INPUT THE DOMAIN"
                printf "PLEASE INPUT THE DOMAIN : "; read domain_name 
        else 
                echo "THE DOMAIN IS "$domain_name 
        fi
}
update_repo () {
        echo "UPDATE UBUNTU REPO"
        sudo apt-get update && sudo apt dist-upgrade -y
}

installing_stuff (){
        echo "INSTALL TOOLS AND DEPEDENCY"
        sudo apt-get install -y curl pwgen git vim zip unzip software-properties-common

        echo "INSTALL NGINX"
        sudo apt-get install -y nginx

        echo "INSTALL MYSQL-SERVER FOR DATABASE"
        sudo apt-get install -y mysql-server

        echo "INSTALL PHP FOR NGINX"
        sudo add-apt-repository -y ppa:ondrej/php
        sudo apt-get install -y php8.0-fpm php8.0-gd php8.0-mbstring php8.0-iconv php8.0-dom php8.0-xml php8.0-zip php8.0-intl php8.0-mysql php8.0-mongodb php8.0-pdo php8.0-curl php8.0-xmlrpc php8.0-soap php8.0-mysql

}

installing_composer(){
        echo "Download composer"
        /usr/bin/curl -sS https://getcomposer.org/installer -o composer-setup.php
        
        echo "Installing composer"
        sudo /usr/bin/php composer-setup.php --install-dir=/usr/local/bin --filename=composer
        
        echo "composer setup"
        rm composer-setup.php
}

configure_db (){

        echo "CREATE DATABASE laravel"
        sudo mysql -u root -e "CREATE DATABASE hameralabdb;"

        echo "GENERATE PASSWORD"
        pass=`pwgen -0 -s -1 32`
        echo $pass

        echo "CREATE USER DB"
        sudo mysql -u root -e "CREATE USER hameralabdb_user@localhost IDENTIFIED BY '${pass}';"

        echo "USER: hameralabdb_user@localhost"
        echo "PASS: $pass"

        echo "GRANT PRIVILEGES"
        sudo mysql -u root -e "GRANT ALL PRIVILEGES ON hameralabdb_user.* TO hameralabdb_user@localhost;"

        echo "FLUSH PRIVILEGES "
        sudo mysql -u root -e "FLUSH PRIVILEGES;"
}

configure_nginx (){
        domain_config=/etc/nginx/sites-available/$domain_name.conf 
        default_config=/etc/nginx/sites-enabled/default

        echo "CREATE NGINX CONFIG"
        touch "$domain_name".conf
        cat > "$domain_name".conf << scripting
server {
        listen 80 default_server;
        listen [::]:80 default_server;
                
        root /var/www/${domain_name}/public;
                
        index index.php index.html index.htm index.nginx-debian.html;
                
        server_name ${domain_name};
        
        add_header X-Frame-Options "SAMEORIGIN";
        add_header X-XSS-Protection "1, mode=block";
        add_header X-Content-Type-Options "nosniff";
        
        charset   utf-8;
        access_log /var/log/nginx/${domain_name}.access.log;
        error_log /var/log/nginx/${domain_name}.error.log error;
        gzip on;
        gzip_vary on;
        gzip_disable "msie6";
        gzip_comp_level 6;
        gzip_min_length 1100;
        gzip_buffers 16 8k;
        gzip_proxied any;
        gzip_types
        text/plain
        text/css
        text/js
        text/xml
        text/javascript
        application/javascript
        application/x-javascript
        application/json
        application/xml
        application/xml+rss;

scripting
        cat >> "$domain_name".conf << "scripting"
        location / {
                try_files $uri $uri/ /index.php?$query_string;
        }
        location ~ \.php$ {
                try_files $uri /index.php =404;
                fastcgi_split_path_info ^(.+\.php)(/.+)\$;
                fastcgi_pass unix:/run/php/php8.0-fpm.sock;
                fastcgi_read_timeout 1200;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                include fastcgi_params;
        }
        location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc|svg|woff|woff2|ttf)$ {
                expires 1M;
                access_log off;
                add_header Cache-Control "public";
        }
        location ~* \.(?:css|js)$ {
                expires 7d;
                access_log off;
                add_header Cache-Control "public";
        }
        location ~ /\.ht {
                deny  all;
        }
}
scripting

        if [ -f "$domain_config" ]
        then
                echo "$domain_name already created"
                rm $domain_name.conf
        else
                sudo mv "$domain_name".conf /etc/nginx/sites-available/
                sudo ln -s /etc/nginx/sites-available/"$domain_name".conf /etc/nginx/sites-enabled/
        fi

        if [ -f "$default_config" ]
        then 
                sudo unlink /etc/nginx/sites-enabled/default
                echo "default config removed"
        else
                echo "default config already removed"
        fi

        echo "RESTART NGINX"
        sudo systemctl restart nginx
}

main () {
        check_domain
        update_repo
        installing_stuff
        installing_composer
        configure_nginx
        configure_db
}
main