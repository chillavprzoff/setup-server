#!/bin/bash

while true; do
  ansible-playbook -i ./$INVENTORY_FILE ./$PLAYBOOK_FILE
  
  read -p 'retry? (Y/n): ' RETRY
  if [[ $RETRY == "n" ]]; then
      break
  fi

done
