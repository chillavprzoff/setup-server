#!/bin/bash

source ./scripts/general-setup.sh

source ./scripts/add-ssh-port.sh

source ./scripts/add-more-servers.sh

source ./scripts/play-the-playbook.sh
