#!/bin/bash
while [[ true ]]; do
  read -p "Add More Server? (Y/n): " ADDMORE
  if [[ $ADDMORE == "n" ]] || [[ $ADDMORE == "N" ]]; then
    break
  else
    read -p "Single IP or Multiple(Bulk)? Answer S/s|single M/m|multiple: " ADD_SERVER_BULK_OR_SINGLE
    if [[ $ADD_SERVER_BULK_OR_SINGLE == "S" ]] || [[ $ADD_SERVER_BULK_OR_SINGLE == "s" ]]; then
      read -p 'Server IP : ' IPSERVER
      yq e e -i '.servers.hosts["'$IPSERVER'"] = null' ./$INVENTORY_FILE
    elif [[ $ADD_SERVER_BULK_OR_SINGLE == "M" ]] || [[ $ADD_SERVER_BULK_OR_SINGLE == "m" ]]; then
      echo "# !!! Remove this line and Paste the IP of servers you want to add !!!" > ./temphosts
      vi ./temphosts
      SERVER_HOSTS=($(tr '\n' ' ' < ./temphosts))
      for s in ${SERVER_HOSTS[@]} ; do
        yq e e -i '.servers.hosts["'$s'"] = null' ./$INVENTORY_FILE
      done
    fi
  fi
done
