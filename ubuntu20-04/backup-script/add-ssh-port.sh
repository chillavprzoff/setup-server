#!/bin/bash
read -p "Connect with non-general ssh port (22)? (Y/n): " CHANGE_SSH
if [[ $CHANGE_SSH != "n" ]];then 
  if [[ $CHANGE_SSH != "N" ]]; then
    read -p 'SSH Port : ' SSH_PORT
    echo "    ansible_ssh_port: $SSH_PORT" >> ./$INVENTORY_FILE
  fi
fi
