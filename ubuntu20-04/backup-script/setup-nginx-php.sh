#!/bin/bash

source ./scripts/general-setup.sh
source ./scripts/add-ssh-port.sh
source ./scripts/add-more-servers.sh

if [[ $PHP_VERSION == "" ]]; then
  source ./scripts/choose-php-version.sh
fi

sed -e "s/PHP_VERSION/$PHP_VERSION/" ./nginx-php.yaml > ./yamls/nginx-php.yaml

read -p "Are you want php extention for pgsql? (y/N): " EXTENTION_PGSQL
if [[ $EXTENTION_PGSQL == "Y" ]] || [[ $EXTENTION_PGSQL == "y" ]]; then
  sed -i "s/#- $PHP_VERSION-pgsql/- $PHP_VERSION-pgsql/" ./yamls/nginx-php.yaml
fi
read -p "Are you want php extention for mysql? (y/N): " EXTENTION_MYSQL
if [[ $EXTENTION_MYSQL == "Y" ]] || [[ $EXTENTION_MYSQL == "y" ]]; then
  sed -i "s/#- $PHP_VERSION-mysql/- $PHP_VERSION-mysql/" ./yamls/nginx-php.yaml
fi
read -p "Are you want php extention for mongo? (y/N): " EXTENTION_MONGO
if [[ $EXTENTION_MONGO == "Y" ]] || [[ $EXTENTION_MONGO == "y" ]]; then
  sed -i "s/#- $PHP_VERSION-mongodb/- $PHP_VERSION-mongodb/" ./yamls/nginx-php.yaml
fi
read -p "Are you want php extention for redis? (y/N): " EXTENTION_REDIS
if [[ $EXTENTION_REDIS == "Y" ]] || [[ $EXTENTION_REDIS == "y" ]]; then
  sed -i "s/#- $PHP_VERSION-redis/- $PHP_VERSION-redis/" ./yamls/nginx-php.yaml
fi

INVENTORY_FILE="inventory.yaml"
PLAYBOOK_FILE="yamls/nginx-php.yaml"
source ./scripts/play-the-playbook.sh
