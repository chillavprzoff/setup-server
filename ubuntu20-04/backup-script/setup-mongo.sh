#!/bin/bash

source ./scripts/general-setup.sh
source ./scripts/add-ssh-port.sh

while true; do
  ansible-playbook -i ./inventory.yaml ./mongo.yaml
  
  read -p 'retry? (Y/n): ' RETRY
  if [[ $RETRY == n ]]; then
    break
  fi
done
