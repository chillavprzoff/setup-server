#!/bin/bash

source ./scripts/add-bastion.sh

if [[ $IPSERVER == "" ]]; then
  read -p 'Server IP: ' IPSERVER
fi
if [[ $USER_ANSIBLE == "" ]]; then
  read -p 'User: ' USER_ANSIBLE
fi

if [[ $SSHPORT == "" ]]; then
  SSHPORT=22
fi

if [[ $BASTION == "n" ]] || [[ $BASTION == "N" ]]; then 
echo "servers:
  hosts:
    $IPSERVER:
  vars:
    ansible_user: $USER_ANSIBLE
    ansible_ssh_private_key_file: '$PRIVATEKEY'
    ansible_ssh_common_args: '-o StrictHostKeyChecking=no'" > ./$INVENTORY_FILE
else
  sed -e "s/SSHPORT/$SSHPORT/" -e "s/HOSTNAME/$HOSTNAME_ANSIBLE/" -e "s/USER\>/$USER_ANSIBLE/" -e "s/IPSERVER/$IPSERVER:/" -e "s/IPBASTION/$IPBASTION/" -e "s/USER_BASTION/$USER_BASTION/" ./inventory.yaml-example > ./$INVENTORY_FILE
fi
