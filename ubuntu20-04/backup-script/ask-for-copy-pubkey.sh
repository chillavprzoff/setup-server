#!/bin/bash

read -p 'Path to your Public Key (default=~/.ssh/id_rsa.pub): ' PUBKEY

PUBKEY=$(<~/.ssh/id_rsa.pub)

if [[ $PUBKEY == "" ]]; then
  echo "Cannot read public key, plese make sure the path is correct!"
  echo "Exiting.."
  exit
else
  echo "    PUBLIC_KEY: '$PUBKEY'" >> ./$INVENTORY_FILE
fi
