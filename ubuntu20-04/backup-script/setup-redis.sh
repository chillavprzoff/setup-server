#!/bin/bash

source ./scripts/general-setup.sh

REDIS_PASSWORD=$(openssl rand -base64 32 | tr -d "=+/" | cut -c1-25)
sed -i "5 i \ \ \ \ REDIS_PASSWORD: '$REDIS_PASSWORD'" ./inventory.yaml

while true; do
  ansible-playbook -i ./inventory.yaml ./redis.yaml
  echo ""
  echo "-----------------------------------------"
  echo "REDIS_PASSWORD: $REDIS_PASSWORD"
  echo "-----------------------------------------"
  echo ""
  
  read -p 'retry? (Y/n): ' RETRY
  if [[ $RETRY == n ]]; then
    break
  fi
done
