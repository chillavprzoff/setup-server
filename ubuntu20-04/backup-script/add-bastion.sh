#!/bin/bash

if [[ $IPBASTION == "" ]]; then
  read -p 'Use Bastion? (Y/n): ' BASTION
  if [[ $BASTION == "" ]] || [[ $BASTION == "y" ]] || [[ $BASTION == "Y" ]]; then
    read -p 'Bastion IP: ' IPBASTION
    if [[ $SSHPORT == "" ]]; then
      read -p 'Bastion SSH Port: ' SSHPORT
    fi
    if [[ $USER_BASTION == "" ]]; then
      read -p 'User Bastion (default=ec2-user): ' USER_BASTION
      if [[ $USER_BASTION == "" ]]; then
        USER_BASTION="ec2-user"
      fi
    fi
  fi
fi
