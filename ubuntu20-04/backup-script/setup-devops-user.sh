#!/bin/bash

source ./scripts/general-setup.sh
source ./scripts/add-ssh-port.sh
source ./scripts/ask-for-access-server.sh
source ./scripts/ask-for-copy-pubkey.sh
source ./scripts/add-more-servers.sh

INVENTORY_FILE="inventory.yaml"
PLAYBOOK_FILE="create-devops-user.yaml"
source ./scripts/play-the-playbook.sh
