#!/bin/bash
while true; do
  read -p 'Sudo Password : ' SUDO_PASSWORD
  if [[ $SUDO_PASSWORD == "" ]]; then
    echo Please enter sudo password!
    echo "" 
  fi
done
echo "    ansible_sudo_pass: '$SUDO_PASSWORD'" >> ./$INVENTORY_FILE
