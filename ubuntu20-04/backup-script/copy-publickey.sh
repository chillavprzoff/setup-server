#!/bin/bash

INVENTORY_FILE="inventory.yaml"

read -p 'Path to Secret Key (default=~/.ssh/id_rsa): ' SECKEY
source ./scripts/general-setup.sh
source ./scripts/add-ssh-port.sh

read -p "Are you want to set hostname? (Y/n): " SET_HOSTNAME
if [[ $SET_HOSTNAME != "n" ]]; then
  source ./scripts/insert-hostname.sh
  APP_NAME=$HOSTNAME_ANSIBLE
fi

if [[ $SECKEY == "" ]]; then
  SECKEY="~/.ssh/id_rsa"
fi

read -p 'Path to your Public Key (default=~/.ssh/id_rsa.pub): ' PUBKEY

if [[ $PUBKEY == "" ]]; then
  PUBKEY=$(<~/.ssh/id_rsa.pub)
else
  PUBKEY=$(<$PUBKEY)
fi

if [[ $PUBKEY == "" ]]; then
  echo "Cannot read public key, plese make sure the path is correct!"
  echo "Exiting.."
  exit
fi

echo "    ansible_ssh_private_key_file: $SECKEY" >> ./$INVENTORY_FILE
echo "    PUBLIC_KEY: '$PUBKEY'" >> ./$INVENTORY_FILE

source ./scripts/add-more-servers.sh

while true; do
  if [[ $HOSTNAME_ANSIBLE == "" ]]; then
    ansible-playbook -i ./$INVENTORY_FILE ./copy-publickey.yaml
  else
    ansible-playbook -i ./$INVENTORY_FILE ./copy-publickey-hostname.yaml
  fi

  source ./scripts/addentry-sshconfig.sh
  
  read -p 'retry? (Y/n): ' RETRY
  if [[ $RETRY == n ]]; then
    break
  fi
done
