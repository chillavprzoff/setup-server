db.createUser(
  {
    user: "admin",
    pwd: "MONGO_ADMIN_PASS",
    roles: [{role: "userAdminAnyDatabase", db: "admin"}]
  }
)
