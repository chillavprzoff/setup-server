#!/bin/bash

source ./scripts/general-setup.sh
source ./scripts/add-ssh-port.sh

MONGO_ADMIN_PASS=$(openssl rand -base64 32 | tr -d "=+/" | cut -c1-25)
read -p "MongoDB User : " MONGO_USER
MONGO_USER_PASS=$(openssl rand -base64 32 | tr -d "=+/" | cut -c1-25)
read -p "MongoDB User DB : " MONGO_USER_DB

sed -i "5 i \ \ \ \ MONGO_ADMIN_PASS: '$MONGO_ADMIN_PASS'" ./inventory.yaml
# sed -i "5 i \ \ \ \ MONGO_USER: '$MONGO_USER'" ./inventory.yaml
# sed -i "5 i \ \ \ \ MONGO_USER_PASS: '$MONGO_USER_PASS'" ./inventory.yaml
# sed -i "5 i \ \ \ \ MONGO_USER_DB: '$MONGO_USER_DB'" ./inventory.yaml

sed "s/MONGO_ADMIN_PASS/$MONGO_ADMIN_PASS/" ./scripts/mongo-setup-admin-user.js > ./scripts/temp/mongo-setup-admin-user.js
sed "s/MONGO_USER/$MONGO_USER/" ./scripts/mongo-setup-user.js > ./scripts/temp/mongo-setup-user.js
sed -i "s/MONGO_PASS_USER/$MONGO_USER_PASS/" ./scripts/temp/mongo-setup-user.js
sed -i "s/MONGO_DB_USER/$MONGO_USER_DB/" ./scripts/temp/mongo-setup-user.js

while true; do
  ansible-playbook -i ./inventory.yaml ./mongodb.yaml

  echo ""
  echo "-----------------------------------------------"
  echo "MONGO_ADMIN: admin"
  echo "-----------------------------------------------"
  echo "MONGO_ADMIN_PASS: $MONGO_ADMIN_PASS"
  echo "-----------------------------------------------"
  echo "MONGO_USER: $MONGO_USER"
  echo "-----------------------------------------------"
  echo "MONGO_USER_PASS: $MONGO_USER_PASS"
  echo "-----------------------------------------------"
  echo "MONGO_USER_DB: $MONGO_USER_DB"
  echo "-----------------------------------------------"
  echo ""
  
  read -p 'retry? (Y/n): ' RETRY
  if [[ $RETRY == n ]]; then
    break
  fi
done
