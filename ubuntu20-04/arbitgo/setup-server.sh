#!/bin/bash

if [[ ! -d ./yamls ]]; then
  mkdir ./yamls
fi
if [[ ! -d ./envs ]]; then
  mkdir ./envs
fi
if [[ ! -d ./hosts ]]; then
  mkdir ./hosts
fi

if [[ -f ./hosts.temp ]] && [[ -f ./env.temp ]]; then
  CURRENT_WORKING_APP=$(cat ./env.temp | grep "APP_NAME" | sed "s/APP_NAME: //g" | sed 's/[[:space:]]//g' )
  
  echo "There's Current Domain find in env.yaml = $CURRENT_WORKING_APP"
  read -p "You want to setup up new domain? (y/N): " RESETUP
fi

PS3='Please enter app your choice: '
options=("Arbit-FE" "Arbit-FE-Main" "Backoffice-PT" "Backoffice2-PT" "Arbit-API" "Trade-FE" "Trade-API" "DEX-APP" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Arbit-FE")
            APP="arbit-fe"
            PLAYBOOK="arbit-app"
            break
            ;;
        "Arbit-FE-Main")
            APP="arbit-fe"
            PLAYBOOK="arbit-app-main"
            break
            ;;
        "Backoffice-PT")
            APP="backoffice-pt"
            PLAYBOOK="bo-pt"
            break
            ;;
        "Backoffice2-PT") 
            APP="backoffice-pt"
            PLAYBOOK="bo2-pt"
            break
            ;;
        "Arbit-API") 
            APP="arbit-api"
            break
            ;;
        "Trade-FE")
            PLAYBOOK="trade-app"
            APP="arbit-trade"
            break
            ;;
        "Trade-API")
            APP="trade-api"
            break
            ;;
        "DEX-APP")
            APP="dex-app"
            break
            ;;
        "Quit")
            exit
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

if [[ $RESETUP == "y" ]] || [[ $RESETUP == "Y" ]] || [[ ! -f ./hosts.temp ]]; then

  read -p 'You want to setup bastion first? (y/N): ' BASTION
  if [[ $BASTION == y ]]; then
    read -p 'Domain Name #doNotTypo: ' DNAME
    read -p 'Bastion IP: ' IPBASTION
    read -p 'Path to Bastion Private Key: ' BASTIONKEY

    chmod 600 $BASTIONKEY

    read -p 'Path to your Public Key (default=~/.ssh/id_rsa.pub): ' PUBKEY

    PUBKEY=$(<~/.ssh/id_rsa.pub)
    
    if [[ $PUBKEY == "" ]]; then
      echo "Cannot read public key, plese make sure the path is correct!"
      echo "Exiting.."
      exit
    fi

    read -p 'Do you want to change ssh port? (port/N): ' SSHPORT

    echo "[bastion]
$IPBASTION

[bastion:vars]
ansible_user=ec2-user
ansible_ssh_private_key_file=$BASTIONKEY" > ./hosts/bastion

    echo "APP_NAME: "${DNAME}"
PUBLIC_KEY: "$PUBKEY"" > ./envs/bastion.yaml

    if [[ $SSHPORT != "" ]]; then
      :
    fi

    while true; do
      ansible-playbook -i ./hosts/bastion ./bastion.yaml
      
      read -p 'retry? (Y/n): ' RETRY
      if [[ $RETRY == n ]]; then
        break
      fi
    done

  fi

  if [[ $DNAME == "" ]]; then
    read -p 'Domain Name #doNotTypo: ' DNAME
  fi
  if [[ $IPBASTION == "" ]]; then
    read -p 'Bastion IP: ' IPBASTION
  fi

  read -p 'Server IP ('$APP'): ' IPSERVER
  read -p 'Arbit DB: ' DBARBIT
  read -p 'Trade DB: ' DBTRADE

  echo "APP_NAME: "${DNAME}"
APP: "${APP}"

DB_HOST_YUNET: "${DBARBIT}"

DB_HOST_YUCLICK: "${DBTRADE}"" | tee ./yamls/env-$DNAME-$APP.yaml > ./env.temp

  sed -e "s/IPSERVER/${IPSERVER}/" -e "s/IPBASTION/${IPBASTION}/" ./hosts-example > ./hosts/$DNAME-$APP
  cat ./hosts/$DNAME-$APP > ./hosts.temp

else

  read -p 'Server IP ('$APP'): ' IPSERVER

  sed '2d' ./hosts.temp > ./hosts/.hosts
  sed '2i\'$IPSERVER'' -i ./hosts/.hosts
  sed "s/APP: .*/APP: ${APP}/" ./env.temp > ./envs/.env
  mv -v ./hosts/.hosts ./hosts/$CURRENT_WORKING_APP-$APP
  mv -v ./envs/.env ./yamls/env-$CURRENT_WORKING_APP-$APP.yaml

  USECURRENT=true
  
fi

if [[ $PLAYBOOK == "" ]]; then
  PLAYBOOK=$APP
fi

if [[ $USECURRENT == true ]]; then
    sed "s/\"env.yaml\"/env-$CURRENT_WORKING_APP-$APP.yaml/" ./$PLAYBOOK.yaml > ./yamls/$CURRENT_WORKING_APP-$APP.yaml
  else
    sed "s/\"env.yaml\"/env-$DNAME-$APP.yaml/" ./$PLAYBOOK.yaml > ./yamls/$DNAME-$APP.yaml
fi

while true; do
  if [[ $USECURRENT == true ]]; then
    ansible-playbook -i ./hosts/$CURRENT_WORKING_APP-$APP ./yamls/$CURRENT_WORKING_APP-$APP.yaml
  else
    ansible-playbook -i ./hosts/$DNAME-$APP ./yamls/$DNAME-$APP.yaml
  fi
  
  read -p 'retry? (Y/n): ' RETRY
  if [[ $RETRY == n ]]; then
    exit
  fi
done
