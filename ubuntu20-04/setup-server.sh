#!/bin/bash

cd "$(dirname "$0")"

if [[ -f .tmpenv ]]; then
  rm -rf .tmpenv
fi

while [[ true ]]; do

  PS3='Please choose playbook you want to play: '
  options=("Quit" "Setup Bastion" "Copy SSH key public" "Install Laravel Setup CLI" "Setup nginx and PHP" "Setup SSL" "Create Devops User" "Setup Passwordless User" "Setup Redis" "Setup MongoDB" "Install Mongo" "Install Supervisor" "Arbitgo" "Update Upgrade OS" "Close Passowrd SSH" "Setup Docker Engine")
  select opt in "${options[@]}"
  do
      case $opt in
          "Install Laravel Setup CLI")
              source ./scripts/setup-laravel-setup-cli.sh
              break
              ;;
          "Setup nginx and PHP")
              source ./scripts/setup-nginx-php.sh
              break
              ;;
          "Setup SSL")
              source ./scripts/setup-ssl.sh
              break
              ;;
          "Create Devops User")
              source ./scripts/setup-devops-user.sh
              break
              ;;
          "Setup Passwordless User")
              source ./scripts/setup-passwordless.sh
              break
              ;;
          "Copy SSH key public")
              source ./scripts/copy-publickey.sh
              break
              ;;
          "Setup Bastion")
              source ./scripts/setup-bastion.sh
              setupBastion
              break
              ;;
          "Setup Redis")
              source ./scripts/setup-redis.sh
              break
              ;;
          "Setup MongoDB")
              source ./scripts/setup-mongodb.sh
              break
              ;;
          "Install Mongo")
              source ./scripts/setup-mongo.sh
              break
              ;;
          "Install Supervisor")
              source ./scripts/setup-supervisor.sh
              break
              ;;
          "Arbitgo")
              source ./arbitgo/setup-server.sh
              break
              ;; 
          "Update Upgrade OS")
              PLAYBOOK_FILE="update-upgrade-os.yaml"
              source ./scripts/complete-general-setup.sh
              break
              ;;  
          "Close Passowrd SSH")
              PLAYBOOK_FILE="close-password-ssh.yaml"
              source ./scripts/complete-general-setup.sh
              break
              ;; 
          "Setup Docker Engine")
              PLAYBOOK_FILE="setup-docker-engine.yaml"
              source ./scripts/complete-general-setup.sh
              break
              ;; 
          "Quit")
              exit
              ;;
          *) echo "invalid option $REPLY";;
      esac
  done
  
done
