#!/bin/bash
source ./scripts/incomplete-general-setup.sh

read -p "Please enter the timezone of the server [Asia/Jakarta]: " SERVER_TIMEZONE
if [[ $SERVER_TIMEZONE == "" ]]; then
  SERVER_TIMEZONE="Asia/Jakarta"
fi
yq e -i '.'$NAME_HOSTS'.vars.server_timezone = "'$SERVER_TIMEZONE'"' $INVENTORY_FILE

source ./scripts/play-the-playbook.sh
