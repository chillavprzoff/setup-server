#!/bin/bash

source ./scripts/setup-host.sh

function addBastion() {
  SEARCH_HOST="Bastion Host"
  read -p "Search $SEARCH_HOST in ~/.ssh/config? (Y/n): " IS_SEARCH_HOST
  if [[ $IS_SEARCH_HOST != "n" ]] && [[ $IS_SEARCH_HOST != "N" ]]; then
    searchHost;
    setHost;
    IPBASTION=$HOST
    SSHPORT_BASTION=$HOST_SSH_PORT
    USER_BASTION=$HOST_USER
    BASTIONKEY=$HOST_SSH_KEY
    echo "Bastion Host IP: $IPBASTION"
    echo "Bastion Host User: $USER_BASTION"
    echo "Bastion Host SSH Port: $SSHPORT_BASTION"
    echo "Bastion Host Private Key: $BASTIONKEY"
    echo "Success get bastion host detail!"
    clearHost
    echo 
  else
    read -p 'Bastion IP: ' IPBASTION
    if [[ $SSHPORT_BASTION == "" ]]; then
      read -p 'Bastion SSH Port [22]: ' SSHPORT_BASTION
    fi
    if [[ $SSHPORT_BASTION == "" ]]; then
      SSHPORT_BASTION="22"
    fi
    read -p 'Path to Bastion Private Key: ' BASTIONKEY
    chmod 600 $BASTIONKEY
    if [[ $USER_BASTION == "" ]]; then
      read -p 'User Bastion (default=ec2-user): ' USER_BASTION
      if [[ $USER_BASTION == "" ]]; then
        USER_BASTION="ec2-user"
      fi
    fi
  fi
}

function askBastion() {
  if [[ $IPBASTION == "" ]]; then
    read -p 'Use Bastion? (Y/n): ' BASTION
    if [[ $BASTION != "n" ]] && [[ $BASTION != "N" ]]; then
      addBastion
    fi
  fi
}
