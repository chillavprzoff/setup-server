#!/bin/bash

source ./scripts/incomplete-general-setup.sh

read -p "Please enter loki ip/host without http/https (exp: 10.20.30.40 or loki.example.com): " LOKI_HOST 

read -p "Loki port [3100]: " LOKI_PORT 
if [[ $LOKI_PORT == "" ]]; then
  LOKI_PORT=3100
fi

yq e '.[0].roles[0].vars.promtail_config_clients[0].url = "http://'$LOKI_HOST':'$LOKI_PORT'/loki/api/v1/push"' ./playbooks/promtail.yaml > ./tmp/promtail.yaml

PROMTAIL_JOB_TARGETS="localhost"

PROMTAIL_JOB_NUM=0
while true; do
  read -p "Promtail job name: " PROMTAIL_JOB_NAME
  read -p "Promtail job path (exp: /var/log/*log | use correct regex for best result): " PROMTAIL_JOB_PATH
  yq e -i '.[0].roles[0].vars.promtail_config_scrape_configs['$PROMTAIL_JOB_NUM'].job_name = "'$PROMTAIL_JOB_NAME'"' ./tmp/promtail.yaml
  yq e -i '.[0].roles[0].vars.promtail_config_scrape_configs['$PROMTAIL_JOB_NUM'].static_configs[0].labels.job = "'$PROMTAIL_JOB_NAME'"' ./tmp/promtail.yaml
  yq e -i '.[0].roles[0].vars.promtail_config_scrape_configs['$PROMTAIL_JOB_NUM'].static_configs[0].targets[0] = "'$PROMTAIL_JOB_TARGETS'"' ./tmp/promtail.yaml
  yq e -i '.[0].roles[0].vars.promtail_config_scrape_configs['$PROMTAIL_JOB_NUM'].static_configs[0].labels.__path__ = "'$PROMTAIL_JOB_PATH'"' ./tmp/promtail.yaml

  read -p "Are you want to create new job? (y/N): " PROMTAIL_RECREATE_NEW_JOB
  if [[ $PROMTAIL_RECREATE_NEW_JOB == "y" ]] || [[ $PROMTAIL_RECREATE_NEW_JOB == "Y" ]]; then
    yq e -i '.[0].roles[0].vars.promtail_config_scrape_configs['$((PROMTAIL_JOB_NUM + 1))'].job_name = .[0].roles[0].vars.promtail_config_scrape_configs['$PROMTAIL_JOB_NUM']' ./tmp/promtail.yaml
    PROMTAIL_JOB_NUM=$((PROMTAIL_JOB_NUM + 1))
  else
    break
  fi
done

PLAYBOOK_FILE="./tmp/promtail.yaml"
source ./scripts/play-the-playbook.sh
