#!/bin/bash

if [[ $SSH_PASSWORD_CONFIRM == "" ]]; then

	read -p 'Use password to connect to server? (y/n): ' SSH_PASSWORD_CONFIRM
	if [[ $SSH_PASSWORD_CONFIRM == "y" ]]; then
		read -p 'SSH Password: ' SSH_PASSWORD
	else
    read -p 'Use default user private keys? (Y/n): ' SSH_USE_DEFAULT_USER 
    if [[ $SSH_USE_DEFAULT_USER == "n" ]] || [[ $SSH_USE_DEFAULT_USER == "N" ]]; then
      read -p 'Path to Private Key: ' PRIVATEKEY
      echo "    ansible_ssh_private_key_file: '$PRIVATEKEY'" >>./$INVENTORY_FILE
    fi
	fi
	if [[ $SSH_PASSWORD != "" ]]; then
		echo "    ansible_password: '$SSH_PASSWORD'" >>./$INVENTORY_FILE
		echo "    ansible_become_password: '$SSH_PASSWORD'" >>./$INVENTORY_FILE
	fi

fi
