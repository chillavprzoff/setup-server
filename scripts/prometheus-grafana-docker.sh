#!/bin/bash

if [[ ! -d ./tmp/docker/prometheus-grafana/alert-manager ]]; then
  mkdir -p ./tmp/docker/prometheus-grafana/alert-manager
fi

if [[ ! -d ./tmp/prometheus-grafana/prometheus ]] && [[ ! -d ./tmp/prometheus-grafana/grafana ]]; then
  mkdir -p ./tmp/prometheus-grafana/{prometheus,grafana}
fi

source ./scripts/prepare-for-generating-self-sign-cert.sh
source ./scripts/change-port.sh

echo
read -p "External monitor label for prometheus, example='projectname-monitor': " PROMETHEUS_EXTERNAL_MONITOR_LABEL

if [[ $PROMETHEUS_PASSWORD == "" ]] && [[ $PROMETHEUS_ENCYRPT_PASSWORD == "" ]]; then
  HTPASSWD_APP_NAME="prometheus"
  source ./scripts/generate-htpasswd.sh
  PROMETHEUS_PASSWORD=$HTPASSWD_PASSWORD
  PROMETHEUS_ENCYRPT_PASSWORD=$HTPASSWD_ENCYRPT_PASSWORD
fi

if [[ $NODE_EXPORTER_PASSWORD == "" ]] && [[ $NODE_EXPORTER_ENCYRPT_PASSWORD == "" ]]; then
  HTPASSWD_APP_NAME="node-exporter"
  source ./scripts/generate-htpasswd.sh
  NODE_EXPORTER_PASSWORD=$HTPASSWD_PASSWORD
  NODE_EXPORTER_ENCYRPT_PASSWORD=$HTPASSWD_ENCYRPT_PASSWORD
fi

echo "subtitute grafana prometheus username & password.."
if [[ -f ./tmp/docker/prometheus-grafana/docker-compose.yml ]]; then
  yq e -i '.services.grafana.environment."BASIC_AUTH_USER" = "admin"' ./tmp/docker/prometheus-grafana/docker-compose.yml
else
  source ./scripts/node-exporter-port.sh
  yq e '.services.node-exporter.ports[0] = "'$NODE_EXPORTER_PORT':9100"' ./files/docker/prometheus-grafana/docker-compose.yml > ./tmp/docker/prometheus-grafana/docker-compose.yml
fi

changePort "alertmanager" 9093
yq e -i '.services.alertmanager.ports[0] = "'$CHANGED_PORT':9093"' ./tmp/docker/prometheus-grafana/docker-compose.yml
BLACKBOX_DEFAULT_PORT=9115
changePort "blackbox" $BLACKBOX_DEFAULT_PORT
BLACKBOX_PORT=$CHANGED_PORT
yq e -i '.services.blackbox.ports[0] = "'$BLACKBOX_PORT':'$BLACKBOX_DEFAULT_PORT'"' ./tmp/docker/prometheus-grafana/docker-compose.yml
changePort "grafana" 3000
yq e -i '.services.grafana.ports[0] = "'$CHANGED_PORT':3000"' ./tmp/docker/prometheus-grafana/docker-compose.yml
changePort "prometheus" 9090
yq e -i '.services.prometheus.ports[0] = "'$CHANGED_PORT':9090"' ./tmp/docker/prometheus-grafana/docker-compose.yml

yq e -i '.services.grafana.environment."BASIC_AUTH_PASSWORD" = "'$PROMETHEUS_PASSWORD'"' ./tmp/docker/prometheus-grafana/docker-compose.yml 

# subtitute cert in docker-compose
yq e -i '.services.node-exporter.volumes += "./ssl/'$CERT_NAME'-fullchain.pem:/etc/node-exporter/'$CERT_NAME'-fullchain.pem"' ./tmp/docker/prometheus-grafana/docker-compose.yml
yq e -i '.services.node-exporter.volumes += "./ssl/'$CERT_NAME'-cert-key.pem:/etc/node-exporter/'$CERT_NAME'-cert-key.pem"' ./tmp/docker/prometheus-grafana/docker-compose.yml
yq e -i '.services.prometheus.volumes += "./ssl/'$CERT_NAME'-fullchain.pem:/etc/prometheus/'$CERT_NAME'-fullchain.pem"' ./tmp/docker/prometheus-grafana/docker-compose.yml
yq e -i '.services.prometheus.volumes += "./ssl/'$CERT_NAME'-cert-key.pem:/etc/prometheus/'$CERT_NAME'-cert-key.pem"' ./tmp/docker/prometheus-grafana/docker-compose.yml
yq e -i '.services.prometheus.volumes += "./ssl/'$CERT_NAME'-ca.pem:/etc/prometheus/'$CERT_NAME'-ca.pem"' ./tmp/docker/prometheus-grafana/docker-compose.yml

echo "subtitute prometheus username & password.."
yq e '.basic_auth_users.admin = "'$PROMETHEUS_ENCYRPT_PASSWORD'"' ./files/prometheus-grafana/prometheus/web.yml > ./tmp/prometheus-grafana/prometheus/web.yml
yq e -i '.tls_server_config.cert_file = "'$CERT_NAME'-fullchain.pem"' ./tmp/prometheus-grafana/prometheus/web.yml 
yq e -i '.tls_server_config.key_file = "'$CERT_NAME'-cert-key.pem"' ./tmp/prometheus-grafana/prometheus/web.yml 
yq e '(.scrape_configs[] | select(.job_name == "prometheus")).basic_auth.username = "admin"' ./files/docker/prometheus-grafana/prometheus/prometheus.yml > ./tmp/prometheus-grafana/prometheus/prometheus.yml
yq e -i '(.scrape_configs[] | select(.job_name == "prometheus")).basic_auth.password = "'$PROMETHEUS_PASSWORD'"' ./tmp/prometheus-grafana/prometheus/prometheus.yml
yq e -i '(.scrape_configs[] | select(.job_name == "prometheus")).tls_config.ca_file = "'$CERT_NAME'-ca.pem"' ./tmp/prometheus-grafana/prometheus/prometheus.yml
yq e -i '.global.external_labels.monitor= "'$PROMETHEUS_EXTERNAL_MONITOR_LABEL'"' ./tmp/prometheus-grafana/prometheus/prometheus.yml

echo "subtitute node-exporter username & password.."
yq e -i '(.scrape_configs[] | select(.job_name == "node-exporter")).tls_config.ca_file = "'$CERT_NAME'-ca.pem"' ./tmp/prometheus-grafana/prometheus/prometheus.yml
yq e -i '(.scrape_configs[] | select(.job_name == "node-exporter")).basic_auth.username = "prometheus"' ./tmp/prometheus-grafana/prometheus/prometheus.yml
yq e -i '(.scrape_configs[] | select(.job_name == "node-exporter")).basic_auth.password = "'$NODE_EXPORTER_PASSWORD'"' ./tmp/prometheus-grafana/prometheus/prometheus.yml

# Alert Manager
read -p "Alert-Manger telegram bot_token: " TELEGRAM_BOT_TOKEN
read -p "Alert-Manger telegram chat_id : " TELEGRAM_CHAT_ID

yq e '(.receivers[] | select(.name == "telegram-notifications")).telegram_configs[].bot_token = "'$TELEGRAM_BOT_TOKEN'"' ./files/docker/prometheus-grafana/alert-manager/alertmanager.yml > ./tmp/docker/prometheus-grafana/alert-manager/alertmanager.yml
yq e -i '(.receivers[] | select(.name == "telegram-notifications")).telegram_configs[].chat_id = '$TELEGRAM_CHAT_ID'' ./tmp/docker/prometheus-grafana/alert-manager/alertmanager.yml

# Blackbox
sed -i "s/blackbox:.*/blackbox:$BLACKBOX_PORT/" ./tmp/prometheus-grafana/prometheus/prometheus.yml

# Grafana
IFS= read -rd '' output < <(cat ./tmp/ssl/$CERT_NAME-ca.pem)
output=$output yq e '(.datasources[0] | select(.name == "prometheus")).secureJsonData.tlsCACert = strenv(output)' ./files/prometheus-grafana/grafana/datasources.yaml > ./tmp/prometheus-grafana/grafana/datasources.yaml

source ./scripts/node-exporter-targets.sh
