#!/bin/bash

function addIpServer() {
	if [[ $IPSERVER == "" ]] || [[ $ADDMORE_SERVER == true ]]; then
		while [[ true ]]; do
			read -p 'Server IP: ' IPSERVER
			if [[ $IPSERVER == "" ]]; then
				echo "Please enter a Server IP!"
				echo ""
			else
        echo $IPSERVER >>./tmp/ip${NAME_HOSTS}
        sort ./tmp/ip${NAME_HOSTS} | uniq >./tmp/ip${NAME_HOSTS}-uniq
				break
			fi
		done
	fi
}
