#!/bin/bash
function changePort() {
  read -p "Are you want to change default $1 port? [$2] (y/N): " CHANGE_DEFAULT_PORT
  if [[ $CHANGE_DEFAULT_PORT == "y" ]]; then
    while true; do
      read -p "Please enter $1 port: " CHANGED_PORT
      if [[ $CHANGED_PORT == "" ]]; then
        echo "Please enter $1 port!!!"
      else
        break
      fi
    done
  else
    CHANGED_PORT=$2
  fi
}
