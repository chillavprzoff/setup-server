#!/bin/bash

source ./scripts/incomplete-general-setup.sh

if [[ -f ./tmp/docker/prometheus-grafana/docker-compose.yml ]]; then
  rm -rf ./tmp/docker/prometheus-grafana/docker-compose.yml
fi
source ./scripts/prometheus-grafana-docker.sh

source ./scripts/play-the-playbook.sh

if [[ -f ./tmp/docker/prometheus-grafana/docker-compose.yml ]]; then
  rm -rf ./tmp/docker/prometheus-grafana/docker-compose.yml
fi
