#!/bin/bash

while true; do
  SERVER_HOSTS=($(tr '\n' ' ' <./tmp/ip${NAME_HOSTS}-uniq))
  if [[ ${#SERVER_HOSTS[@]} -ge 1 ]]; then
    read -p "You want to add more server or delete some server? (a/A)add (d/D)delete (n/N)no: " ADD_OR_DELETE_SERVER
    if [[ $ADD_OR_DELETE_SERVER == "n" ]] || [[ $ADD_OR_DELETE_SERVER == "N" ]]; then
      break
    elif [[ $ADD_OR_DELETE_SERVER == "a" ]] || [[ $ADD_OR_DELETE_SERVER == "A" ]]; then
      source ./scripts/add-more-servers.sh
    elif [[ $ADD_OR_DELETE_SERVER == "d" ]] || [[ $ADD_OR_DELETE_SERVER == "D" ]]; then
      source ./scripts/delete-servers.sh
    fi
  else
    source ./scripts/add-more-servers.sh
  fi
done
