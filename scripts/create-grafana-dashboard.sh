#!/bin/bash
### Please edit grafana_* variables to match your Grafana setup:
#grafana_host="http://localhost:3000"
#grafana_cred="admin:admin"
#grafana_datasource="prometheus"
#ds=(1860);
#for d in "${ds[@]}"; do
#  echo -n "Processing $d: "
#  j=$(curl -s -k -u "$grafana_cred" $grafana_host/api/gnet/dashboards/$d | jq .json)
#  curl -s -k -u "$grafana_cred" -XPOST -H "Accept: application/json" \
#    -H "Content-Type: application/json" \
#    -d "{\"dashboard\":$j,\"overwrite\":true, \
#        \"inputs\":[{\"name\":\"Resource Monitoring\",\"type\":\"datasource\", \
#        \"value\":\"$grafana_datasource\"}]}" \
#    $grafana_host/api/dashboards/import; echo ""
#done

#!/bin/bash
grafana_host="GRAFANA_URL"
grafana_cred="GRAFANA_USERNAME:GRAFANA_PASSWORD"
grafana_datasource="prometheus"
ds=(GRAFANA_DASHBOARD_IMPORT);
for d in "${ds[@]}"; do
  echo -n "Processing $(curl -s -k -u "$grafana_cred" $grafana_host/api/gnet/dashboards/$d | jq .name), id=$d: "
  curl -s -k -u "$grafana_cred" $grafana_host/api/gnet/dashboards/$d | jq .json | jq '. + {"inputs": .__inputs} | del(.__inputs)' | jq '. + {"dashboard": .}' | jq '{dashboard}' > dashboard.json
  jq '. + {"folderId": 0, "overwrite": true}' dashboard.json > dashboard.tmp && mv dashboard.tmp dashboard.json
  #jq '.dashboard.inputs[0].value = "'$grafana_datasource'"' dashboard.json > dashboard.tmp && mv dashboard.tmp dashboard.json
  #jq '.inputs[0].name = "DS_PROMETHEUS"' dashboard.json > dashboard.tmp && mv dashboard.tmp dashboard.json
  #jq '.inputs[0].type = "datasource"' dashboard.json > dashboard.tmp && mv dashboard.tmp dashboard.json
  #jq '.inputs[0].value = "'$grafana_datasource'"' dashboard.json > dashboard.tmp && mv dashboard.tmp dashboard.json
  #jq '.dashboard.title = "Resource Monitoring"' dashboard.json > dashboard.tmp && mv dashboard.tmp dashboard.json
  sed -i 's/"datasource": "${.*/"datasource": "'$grafana_datasource'",/g' dashboard.json
  curl -s -k -u "$grafana_cred" -XPOST -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d @dashboard.json \
    $grafana_host/api/dashboards/import; echo ""
  #rm -rf dashboard.json
  mv dashboard.json dashboard-$d.json
done
