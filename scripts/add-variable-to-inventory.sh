#!/bin/bash
addVariable() {
  VARIABLE_PROMPT=$1
  VARIABLE_NAME=$2

  VARIABLE=$(yq e '.'${NAME_HOSTS}'.vars.'$VARIABLE_NAME'' ./$INVENTORY_FILE)
  if [[ $VARIABLE != "" ]] && [[ $VARIABLE != "null" ]]; then
    read -p "Variable exists, with value '$VARIABLE', you want to change it? (y/N): " REPLACE_VARIABLE
    if [[ $REPLACE_VARIABLE == "y" ]] || [[ $REPLACE_VARIABLE == "Y" ]]; then
      read -p "$VARIABLE_PROMPT: " VARIABLE
      yq e -i '.'$NAME_HOSTS'.vars.'$VARIABLE_NAME' = "'$VARIABLE'"' $INVENTORY_FILE
    fi
  else
    read -p "$VARIABLE_PROMPT: " VARIABLE
    yq e -i '.'$NAME_HOSTS'.vars.'$VARIABLE_NAME' = "'$VARIABLE'"' $INVENTORY_FILE
  fi

  GENERATED_VARIABLE=$VARIABLE
}
