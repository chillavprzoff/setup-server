#!/bin/bash
useradd -m -s /bin/bash $1
usermod -aG sudo $1
usermod -aG adm $1
usermod -aG dialout $1
usermod -aG cdrom $1
usermod -aG floppy $1
usermod -aG audio $1
usermod -aG dip $1
usermod -aG video $1
usermod -aG plugdev $1
usermod -aG netdev $1
usermod -aG lxd $1

# OLD
#cp /etc/sudoers.d/90-cloud-init-users /etc/sudoers.d/91-$1-users
#sed -i 's/ubuntu/$1/g' /etc/sudoers.d/91-$1-users
#cp /home/ubuntu/.ssh /home/$1/ -r
#chown -R $1:$1 /home/$1/.ssh
# OLD

echo "$1 ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/$1-user
mkdir /home/$1/.ssh
if [[ ! -f /home/$1/.ssh/id_rsa ]]; then
  ssh-keygen -t rsa -P "" -f /home/$1/.ssh/id_rsa -C "$1"
fi
chown -R $1:$1 /home/$1/.ssh
