#!/bin/bash

source ./scripts/general-setup.sh

#source ./scripts/add-sudo-pass.sh

source ./scripts/add-more-servers.sh

sed -e "s/USER/$USER_ANSIBLE/" ./$PLAYBOOK_FILE >./tmp/$(echo $PLAYBOOK_FILE | awk -F "/" '{print $2}')

PLAYBOOK_FILE="./tmp/$(echo $PLAYBOOK_FILE | awk -F "/" '{print $2}')"
source ./scripts/play-the-playbook.sh
