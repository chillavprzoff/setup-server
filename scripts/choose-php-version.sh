#!/bin/bash
PS3='Please choose php version you want to install: '
options=("Quit" "php7.4" "php8.0" "php8.1")
select opt in "${options[@]}"
do
    case $opt in
        "php7.4")
            PHP_VERSION=$opt
            break
            ;;
        "php8.0")
            PHP_VERSION=$opt
            break
            ;;
        "php8.1")
            PHP_VERSION=$opt
            break
            ;;
        "Quit")
            exit
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
