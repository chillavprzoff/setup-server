#!/bin/bash

if [[ $HOSTNAME_ANSIBLE == "" ]]; then
  echo "(Note: dont insert hostname if dont want to set a hostname)"
  read -p 'Hostname: ' HOSTNAME_ANSIBLE
  if [[ $HOSTNAME_ANSIBLE == "" ]]; then
    HOSTNAME_ANSIBLE="-"
  fi
fi

echo "    hostname: '$HOSTNAME_ANSIBLE'" >> ./$INVENTORY_FILE
