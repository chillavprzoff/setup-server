#!/bin/bash
# ref : https://github.com/ChristianLempa/cheat-sheets/blob/main/misc/ssl-certs.md

if [[ "$(docker images -q docker-openssl:latest 2>/dev/null)" == "" ]]; then
	# Build docker-openssl docker image
	if [[ -d ./files/docker-openssl ]]; then
		docker build -f ./files/docker-openssl/Dockerfile -t docker-openssl:latest ./files/docker-openssl
	else
		docker build -f ../files/docker-openssl/Dockerfile -t docker-openssl:latest ../files/docker-openssl
	fi
fi

function getAltName() {
	# $1 = the actual cert
	docker run -it --rm -v $2:/openssl-certs docker-openssl openssl x509 -text -noout -in $1-cert.pem \
		-certopt no_subject,no_header,no_version,no_serial,no_signame,no_validity,no_issuer,no_pubkey,no_sigdump,no_aux
}

function verifySelfSign() {
	# $2 = the actual cert
	docker run -it --rm -v $3:/openssl-certs docker-openssl openssl verify -CAfile $1 -verbose $2
}

function checkSelfSignIsExist() {
	if [[ -f $2/$1-cert.pem ]] && [[ -f $2/$1-ca.pem ]] && [[ -f $2/$1-cert-key.pem ]]; then

		echo "Found certificate at $2: $1-cert.pem, $1-cert-key.pem, $1-ca.pem"
		return 1
	else
		return 0

	fi
}

function generateSelfSign() {
	if [[ $1 == "" ]]; then
		echo "You have to enter the name of certificate!!!"
		return 1
	fi
	if [[ $2 == "" ]]; then
		echo "Subject Alternative Name cannot be empty!!!"
		return 1
	fi
	if [[ $3 == "" ]]; then
		echo "You have to enter the path of certificate!!!"
		return 1
	fi
	if [[ $4 == "" ]]; then
		CERT_EXPIRATION_DAYS=365
	else
		CERT_EXPIRATION_DAYS=$4
	fi
	if [[ $CERT_DOMAIN_NAME == "" ]]; then
		CERT_DOMAIN_NAME=$1
	fi

	if ! checkSelfSignIsExist $1 $3; then
		read -p "Are you want to continue to generate self-sign certs, it will be override the existing certs!!! (Y/n): " CERT_OVERRIDE
		if [[ $CERT_OVERRIDE != "n" ]]; then
			echo "Regenerating certs..."
		else
			echo "Okay, using existing certs..."
			return 0
		fi
	fi

	CERT_CA_KEY_PASSPHRASE=$(cat /dev/urandom | LC_ALL=C tr -dc 'a-zA-Z0-9' | fold -w 50 | head -n 1)

	# Generate CA
	## Generate RSA
	# comment this to create ca-key with passphrase
	docker run -it --rm -v $3:/openssl-certs docker-openssl openssl genrsa -aes256 -out $1-ca-key.pem -passout pass:$CERT_CA_KEY_PASSPHRASE 4096

	## Generate a public CA Cert
	# comment this to create ca-key with passphrase
	docker run -it --rm -v $3:/openssl-certs docker-openssl openssl req -new -x509 -sha256 -days $CERT_EXPIRATION_DAYS -key $1-ca-key.pem -out $1-ca.pem -subj "/O=$1" -passin pass:$CERT_CA_KEY_PASSPHRASE
	# uncomment this to create ca-key without passphrase
	# docker run -it --rm -v $3:/openssl-certs docker-openssl openssl req -nodes -new -x509 -sha256 -days $CERT_EXPIRATION_DAYS -keyout $1-ca-key.pem -out $1-ca.pem -subj "/O=$1"

	# Optional Stage: View Certificate's Content
	docker run -it --rm -v $3:/openssl-certs docker-openssl openssl x509 -in $1-ca.pem -text
	docker run -it --rm -v $3:/openssl-certs docker-openssl openssl x509 -in $1-ca.pem -purpose -noout -text

	# Generate Certificate
	##1. Create a RSA key
	docker run -it --rm -v $3:/openssl-certs docker-openssl openssl genrsa -out $1-cert-key.pem 4096

	## 2. Create a Certificate Signing Request (CSR)
	docker run -it --rm -v $3:/openssl-certs docker-openssl openssl req -new -sha256 -subj "/CN=$CERT_DOMAIN_NAME" -key $1-cert-key.pem -out $1-cert.csr

	## 3. Create a `extfile` with all the alternative names
	# example:
	# echo "subjectAltName=DNS:your-dns.record,IP:257.10.10.1" >> $3/$1-extfile.cnf
	echo "subjectAltName=$2" >>$3/$1-extfile.cnf

	## optional
	## echo extendedKeyUsage = serverAuth >> $3/$1-extfile.cnf

	## 4. Create the certificate
	docker run -it --rm -v $3:/openssl-certs docker-openssl openssl x509 -req -sha256 -days $CERT_EXPIRATION_DAYS -in $1-cert.csr -CA $1-ca.pem -CAkey $1-ca-key.pem -out $1-cert.pem -extfile $1-extfile.cnf -CAcreateserial -passin pass:$CERT_CA_KEY_PASSPHRASE

	## 5. Create fullchain certificate
	cat $3/$1-cert.pem $3/$1-ca.pem >$3/$1-fullchain.pem

	echo
	echo CA Key passphrase: $CERT_CA_KEY_PASSPHRASE
	echo $CERT_CA_KEY_PASSPHRASE >$3/$1-ca-key-passphrase.txt
	read -p "Are you want to archive the certs? (Y/n): " CERT_IS_ARCHIVE
	if [[ $CERT_IS_ARCHIVE != "n" ]]; then
		cd $3
		mkdir certs-$1
		cp -rf $1* certs-$1
		tar -zcvf certs-$1-$(date +%Y_%m_%d).tar.gz certs-$1
		rm -rf certs-$1
		cd -
	fi
}

# example:
# generateSelfSign my-cert-name "DNS:your-dns.record,IP:192.168.1.1" $(pwd)/path/to/cert/directory 3650
