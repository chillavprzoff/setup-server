#!/bin/bash

source ./scripts/general-setup.sh
source ./scripts/add-ssh-port.sh
source ./scripts/add-variable-to-inventory.sh

addVariable "Domain Name" domain_name
DOMAIN_NAME=$GENERATED_VARIABLE
addVariable "Email for Certbot" email

source ./scripts/play-the-playbook.sh

echo ""
echo "----------------------------------------------------"
echo "Installed SSL URL: https://$DOMAIN_NAME"
echo "----------------------------------------------------"
echo ""
