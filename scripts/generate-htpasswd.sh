#!/bin/bash

if [[ "$(docker images -q docker-htpasswd:latest 2> /dev/null)" == "" ]]; then
  # Build docker-htpasswd docker image
  docker build -f ./files/gen-pass/Dockerfile -t docker-htpasswd:latest ./files/gen-pass
fi

echo
read -p "Generate a random password for $HTPASSWD_APP_NAME basic auth? (Y/n): " HTPASSWD_GENERATE_PASSWORD
if [[ $HTPASSWD_GENERATE_PASSWORD == "n" ]]; then
  read -p "Create $HTPASSWD_APP_NAME basic auth password: " HTPASSWD_PASSWORD
else
  HTPASSWD_PASSWORD=$(cat /dev/urandom | LC_ALL=C tr -dc 'a-zA-Z0-9' | fold -w 50 | head -n 1)
fi
HTPASSWD_ENCYRPT_PASSWORD=$(docker run -it --rm docker-htpasswd $HTPASSWD_PASSWORD)
