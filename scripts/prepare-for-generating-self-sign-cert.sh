#!/bin/bash
source ./scripts/generate-self-signed-ssl.sh

### Preparing for generating self-sign ssl ###
read -p "Enter cert name: " CERT_NAME
CERT_PATH_TO_CERT="$(pwd)/tmp/ssl"
yq e -i '.servers.vars.cert_name ="'$CERT_NAME'"' ./$INVENTORY_FILE
if ! checkSelfSignIsExist $CERT_NAME $CERT_PATH_TO_CERT; then
  while [[ true ]]; do
    read -p "Are you want to continue generating certs where the cert with name '$CERT_NAME' already exists? (y/n): " CERT_CONTINUE_IF_CERT_EXIST
    if [[ $CERT_CONTINUE_IF_CERT_EXIST != "n" ]] && [[ $CERT_CONTINUE_IF_CERT_EXIST != "y" ]]; then
      echo "You have to answer 'y' or 'n'!!!"
    elif [[ $CERT_CONTINUE_IF_CERT_EXIST != "n" ]]; then
      read -p "Please enter the domain name for self-sign certs, seperate with space if there are multiple domain name: " CERT_DOMAIN_NAME
      
      i=0
      CERT_DOMAIN_NAME_ARR=()
      for c in ${CERT_DOMAIN_NAME[@]} ; do
        CERT_DOMAIN_NAME_ARR+=($c)
      done

      for d in ${CERT_DOMAIN_NAME_ARR[@]} ; do
        i=$(($i + 1))
        if [[ $i == 1 ]]; then
          CERT_ALTERNATE_NAME="DNS:$d,"
        elif [[ $i != ${#CERT_DOMAIN_NAME_ARR[@]} ]]; then
          CERT_ALTERNATE_NAME="${CERT_ALTERNATE_NAME}DNS:$d,"
        else
          CERT_ALTERNATE_NAME="${CERT_ALTERNATE_NAME}DNS:$d"
        fi
      done
      #CERT_DOMAIN_NAME=$(echo $CERT_DOMAIN_NAME | awk -F " " '{print $1}')
      CERT_DOMAIN_NAME=${CERT_DOMAIN_NAME_ARR[0]}
      echo ini adalah domain name : $CERT_DOMAIN_NAME

      cat ./tmp/ip${NAME_HOSTS}-uniq
      read -p "Are you want to add or remove ip for the self-sign certs? (y/N): " CERT_ADD_OR_DEL 
      if [[ $CERT_ADD_OR_DEL == "y" ]]; then
        CERT_NAME_HOSTS="selfsigncerts"
        cp ./tmp/ip${NAME_HOSTS}-uniq ./tmp/ip${CERT_NAME_HOSTS}-uniq
        cp ./tmp/{ip${CERT_NAME_HOSTS}-uniq,ip${CERT_NAME_HOSTS}}
        NAME_HOSTS=$CERT_NAME_HOSTS
        source ./scripts/add-or-delete-server.sh
      fi
      SERVER_HOSTS=($(tr '\n' ' ' <./tmp/ip${NAME_HOSTS}-uniq))
      for s in ${SERVER_HOSTS[@]} ; do
        CERT_ALTERNATE_NAME="$CERT_ALTERNATE_NAME,IP:$s"
      done
      echo "$CERT_ALTERNATE_NAME"

      echo "Generating self-sign ssl.."
      if [[ ! -d $CERT_PATH_TO_CERT ]]; then
        mkdir -p $CERT_PATH_TO_CERT 
      fi
### End preparing for generating self-sign ssl ###

      CERT_EXPIRATION="3650" # 10 year
      if generateSelfSign $CERT_NAME "$CERT_ALTERNATE_NAME" $CERT_PATH_TO_CERT $CERT_EXPIRATION; then
        getAltName $CERT_NAME $CERT_PATH_TO_CERT
        return 0
      fi

    else
      getAltName $CERT_NAME $CERT_PATH_TO_CERT
      return 0
    fi
    break
  done
fi
