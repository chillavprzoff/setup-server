#!/bin/bash

source ./scripts/incomplete-general-setup.sh

###CUSTOM 

read -p "Swapfile PATH [/swapfile]: " SWAPFILE_PATH
if [[ $SWAPFILE_PATH == "" ]]; then
  SWAPFILE_PATH="/swapfile"
fi

yq e '.[].vars.swap_file = "'$SWAPFILE_PATH'"' ./yamls/setup-swapfile.yaml > ./tmp/setup-swapfile.yaml

read -p "Auto sizing swapfile size? (y/n): " SWAPFILE_SIZE_AUTO
if [[ $SWAPFILE_SIZE_AUTO == "y" ]] || [[ $SWAPFILE_SIZE_AUTO == "Y" ]]; then
  yq e -i '.[].vars.swap_size_mb = "auto"' ./tmp/setup-swapfile.yaml
else
  
  read -p "Swapfile Size in MB: " SWAPFILE_SIZE_MB
  while [[ true ]]; do
    if [[ $SWAPFILE_SIZE_MB == "" ]]; then
      echo Please enter Swapfile Size!
      echo ""
    else
      break
    fi 
  done

  yq e -i ".[].vars.swap_size_mb = $SWAPFILE_SIZE_MB" ./tmp/setup-swapfile.yaml

fi

###CUSTOM 

PLAYBOOK_FILE="tmp/setup-swapfile.yaml"
source ./scripts/play-the-playbook.sh
