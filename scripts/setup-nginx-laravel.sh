#!/bin/bash
#Credit to mas Azwin
#variable
domain_name=$1

choose_php_version() {
  PS3='Please choose php version you want to install: '
  options=("Quit" "php7.4" "php8.0" "php8.1" "php8.2" "php8.3")
  select opt in "${options[@]}"; do
    case $opt in
    "php7.4")
      PHP_VERSION=$opt
      break
      ;;
    "php8.0")
      PHP_VERSION=$opt
      break
      ;;
    "php8.1")
      PHP_VERSION=$opt
      break
      ;;
    "php8.2")
      PHP_VERSION=$opt
      break
      ;;
    "php8.3")
      PHP_VERSION=$opt
      break
      ;;
    "Quit")
      exit
      ;;
    *) echo "invalid option $REPLY" ;;
    esac
  done
}

install_php() {
  sudo apt-get install software-properties-common -y
  sudo add-apt-repository ppa:ondrej/php
  sudo apt-get update -y
  sudo apt install -y $PHP_VERSION-{bcmath,fpm,xml,mysql,zip,intl,ldap,gd,cli,bz2,curl,mbstring,pgsql,opcache,soap,cgi,dev}
}

check_domain() {
  if [ -z $domain_name ]; then
    echo "YOU DIDN'T INPUT THE DOMAIN"
    printf "PLEASE INPUT THE DOMAIN : "
    read domain_name
  else
    echo "THE DOMAIN IS "$domain_name
  fi
}

configure_db() {

  mysql_exists=$(which mysql)
  if [[ $mysql_exists == "" ]]; then
    echo "INSTALL MYSQL-SERVER FOR DATABASE"
    sudo apt-get install -y mysql-server
  fi

  read -p "Insert database name: " DB_NAME
  read -p "Insert database username: " DB_USERNAME
  echo "CREATE DATABASE laravel"
  sudo mysql -u root -e "CREATE DATABASE $DB_NAME;"

  echo "GENERATE PASSWORD"
  # pass=`pwgen -0 -s -1 32`
  DB_DB_PASS=$(openssl rand -base64 32 | tr -d "=+/" | cut -c1-25)
  echo $DB_DB_PASS

  echo "CREATE USER DB"
  sudo mysql -u root -e "CREATE USER $DB_USERNAME@localhost IDENTIFIED BY '${DB_DB_PASS}';"

  echo ""
  echo "----------------------------"
  echo "USER: $DB_USERNAME@localhost"
  echo "DB_PASS: $DB_PASS"
  echo "DB: $DB_NAME"
  echo "----------------------------"
  echo ""

  echo "GRANT PRIVILEGES"
  sudo mysql -u root -e "GRANT ALL PRIVILEGES ON $DB_NAME.* TO $DB_USERNAME@localhost;"

  echo "FLUSH PRIVILEGES "
  sudo mysql -u root -e "FLUSH PRIVILEGES;"
}

configure_nginx() {
  domain_config=/etc/nginx/sites-available/$domain_name.conf
  default_config=/etc/nginx/sites-enabled/default

  echo "CREATE NGINX CONFIG"
  sudo echo 'server {
  listen 80 default_server;
  listen [::]:80 default_server;
          
  root /var/www/'${domain_name}'/public;
          
  index index.php index.html index.htm index.nginx-debian.html;
          
  server_name '${domain_name}';
  
  add_header X-Frame-Options "SAMEORIGIN";
  add_header X-XSS-Protection "1, mode=block";
  add_header X-Content-Type-Options "nosniff";
  
  charset   utf-8;
  access_log /var/log/nginx/'${domain_name}'.access.log;
  error_log /var/log/nginx/'${domain_name}'.error.log error;
  gzip on;
  gzip_vary on;
  gzip_disable "msie6";
  gzip_comp_level 6;
  gzip_min_length 1100;
  gzip_buffers 16 8k;
  gzip_proxied any;
  gzip_types
  text/plain
  text/css
  text/js
  text/xml
  text/javascript
  application/javascript
  application/x-javascript
  application/json
  application/xml
  application/xml+rss;

  location / {
      try_files $uri $uri/ /index.php?$query_string;
  }
  location ~ \.php$ {
      try_files $uri /index.php =404;
      fastcgi_split_path_info ^(.+\.php)(/.+)\$;
      fastcgi_pass unix:/run/php/'$PHP_VERSION'-fpm.sock;
      fastcgi_read_timeout 1200;
      fastcgi_index index.php;
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
      include fastcgi_params;
  }
  location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc|svg|woff|woff2|ttf)$ {
      expires 1M;
      access_log off;
      add_header Cache-Control "public";
  }
  location ~* \.(?:css|js)$ {
      expires 7d;
      access_log off;
      add_header Cache-Control "public";
  }
  location ~ /\.ht {
      deny  all;
  }
}' >$domain_name.conf

  if [ -f "$domain_config" ]; then
    echo "$domain_name already created"
    sudo rm $domain_name.conf
  else
    sudo mv "$domain_name".conf /etc/nginx/sites-available/
    sudo ln -s /etc/nginx/sites-available/"$domain_name".conf /etc/nginx/sites-enabled/
  fi

  if [ -f "$default_config" ]; then
    sudo unlink /etc/nginx/sites-enabled/default
    echo "default config removed"
  else
    echo "default config already removed"
  fi

  echo "RESTART NGINX"
  if sudo nginx -t; then
    sudo nginx -s reload
  fi
}

setup_repo() {
  read -p "Repo URL without https: " REPO_URL
  read -p "Repo User: " REPO_USER
  read -p "Repo Password: " REPO_PASS

  PS3='Please choose environment deployement: '
  options=("development" "staging" "production")
  select opt in "${options[@]}"; do
    case $opt in
    "development")
      ENVIRONMENT=$opt
      break
      ;;
    "staging")
      ENVIRONMENT=$opt
      break
      ;;
    "production")
      ENVIRONMENT=$opt
      break
      ;;
    "Insert manual")
      read -p "Environment: " ENVIRONMENT
      break
      ;;
    *) echo "invalid option $REPLY" ;;
    esac
  done
  sudo git clone -b $ENVIRONMENT https://$REPO_USER:$REPO_PASS@$REPO_URL /var/www/$domain_name
  sudo chown -R $USER:$USER /var/www/$domain_name

  read -p "configure environment? (Y/n): " CON_ENV
  if [[ $CON_ENV != "n" ]]; then
    cp /var/www/$domain_name/.env.example /var/www/$domain_name/.env
    if [[ $CONFIG_MYSQL != "n" ]]; then
      sed -i "s/DB_DATABASE=.*/DB_DATABASE=$DB_NAME/" /var/www/$domain_name/.env
      sed -i "s/DB_USERNAME=.*/DB_USERNAME=$DB_USERNAME/" /var/www/$domain_name/.env
      sed -i "s/DB_PASSWORD=.*/DB_PASSWORD=$DB_USERNAME/" /var/www/$domain_name/.env
    fi

    read -p "You want to open env file using vim or nano? (default:vim) answer : n to use nano : " EDIT_VIM
    read -p "make sure you configure environment corectly, if you need console please open another console, this session must be finish before you can use this console, press any key to confinue.." CONFIRM
    if [[ $EDIT_VIM == "n" ]]; then
      nano /var/www/$domain_name/.env
    else
      vi /var/www/$domain_name/.env
    fi
  fi

  cd /var/www/$domain_name

  read -p "Are you want to do composer install? (Y/n) : " COMPOSER_INSTALL
  if [[ $COMPOSER_INSTALL != "n" ]]; then
    composer_install
    read -p "You want to do php artisan migrate ? (Y/n): " ARTISAN_MIGRATE
    if [[ $ARTISAN_MIGRATE != "n" ]]; then
      $PHP_VERSION artisan migrate
    fi
    read -p "You want to do php artisan db:seed ? (Y/n): " ARTISAN_SEED
    if [[ $ARTISAN_SEED != "n" ]]; then
      $PHP_VERSION artisan db:seed
    fi
    read -p "You want to do php artisan key:generate ? (Y/n): " KEY_GENERATE
    if [[ $KEY_GENERATE != "n" ]]; then
      $PHP_VERSION artisan key:generate
    fi
  fi

  cd
}

composer_install() {
  cd /var/www/$domain_name
  composer install
  cd
}

main() {
  choose_php_version

  read -p "Are you want to install php based on php version you choosed before? (Y/n) : " INSTALL_PHP
  if [[ $INSTALL_PHP != "n" ]]; then
    install_php
  fi

  check_domain

  read -p "Are you want to configure mysql database? (Y/n) : " CONFIG_MYSQL
  if [[ $CONFIG_MYSQL != "n" ]]; then
    configure_db
  fi
  read -p "Are you want to setup repo? (Y/n) : " SETUP_REPO
  if [[ $SETUP_REPO != "n" ]]; then
    setup_repo
  fi

  read -p "Are you want to change web root owner to www-data? (Y/n) : " CHOWN_WWW
  if [[ $CHOWN_WWW != "n" ]]; then
    sudo chown -R www-data:www-data /var/www/$domain_name
  fi

  configure_nginx

  read -p "Are you want to request ssl certificate from let'sencrypt? (Y/n) : " INSTALL_SSL
  if [[ $INSTALL_SSL != "n" ]]; then
    CERTBOT=$(which certbot)
    if [[ $CERTBOT == "" ]]; then
      echo "Installing certbot.."
      echo "Make sure snap is up to date .."
      sudo snap install core
      sudo snap refresh core
      sudo snap install --classic certbot
    fi
    read -p "Your Email: " EMAIL
    if sudo certbot --nginx --redirect -d $domain_name -m $EMAIL --no-eff-email --agree-tos; then
      echo "Test automatic renewal"
      sudo certbot renew --dry-run
    fi

  fi
}
main
