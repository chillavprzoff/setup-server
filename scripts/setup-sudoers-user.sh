#!/bin/bash

source ./scripts/general-setup.sh
source ./scripts/add-ssh-port.sh
source ./scripts/ask-for-copy-pubkey.sh
source ./scripts/add-more-servers.sh

read -p "Sudoers Username: " USERNAME
echo "    USERNAME: '$USERNAME'" >> ./$INVENTORY_FILE

if source ./scripts/play-the-playbook.sh; then
  USER_ANSIBLE="$USERNAME"
  source ./scripts/addentry-sshconfig.sh
fi
