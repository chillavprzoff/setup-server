#!/bin/bash

mem=$(free -m | awk '/^Mem:/{print $2}')  # mendapatkan ukuran RAM dalam MB
swap=$(free -m | awk '/^Swap:/{print $2}')  # mendapatkan ukuran swapfile dalam MB

if [[ $mem -lt 2048 ]]; then  # jika RAM kurang dari 2GB
  if [[ $swap -lt $((mem * 2)) ]]; then  # jika swapfile kurang dari 2 kali ukuran RAM
    sudo dd if=/dev/zero of=/swapfile bs=1M count=$((mem * 2))  # membuat swapfile baru
  fi
elif [[ $mem -ge 2048 && $mem -le 8192 ]]; then  # jika RAM antara 2GB dan 8GB
  if [[ $swap -lt $mem ]]; then  # jika swapfile kurang dari ukuran RAM
    sudo dd if=/dev/zero of=/swapfile bs=1M count=$mem  # membuat swapfile baru
  fi
else  # jika RAM lebih dari 8GB
  if [[ $swap -lt 4096 ]]; then  # jika swapfile kurang dari 4GB
    sudo dd if=/dev/zero of=/swapfile bs=1M count=4096  # membuat swapfile baru
  fi
fi
