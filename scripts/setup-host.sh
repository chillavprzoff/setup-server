#!/bin/bash
function searchHost() {
if [[ $QUIET_MODE != true ]]; then
  while true
  do

    if [[ $HOST_TEMP != "" ]]; then
      HOST=$HOST_TEMP
    else
      read -p "Target Host in ssh config : " HOST
      HOST_TEMP=$HOST
    fi

    PS3='Please choose '$SEARCH_HOST' from ssh config: '
    options=($(uniq ~/.ssh/config | grep $HOST | sed 's/Host//g' | sed 's/ProxyCommand//g' | sed 's/ssh//g' | sed 's/-q//g' | sed 's/-W//g' | sed 's/%h:%p.*//g' | sed 's/User.*//g'))

    if [[ $options == "" ]]; then
      echo "Host not found! please insert again, or crtl-c to exit."
    fi
    HOST_TEMP=""

    select opt in "${options[@]}" 
    do
      HOST=$opt
      options=$HOST
      break
    done

    if [[ $options == $HOST ]]; then
      break
    fi

  done
fi
}

function setHost(){
  if [[ $QUIET_MODE != true ]]; then
    HOST_NAME_SSH=$HOST
    HOST_NAME=$(echo $HOST | sed 's/\-/\\\-/')
    GET_NUMBER=($(grep -n -E "(^|\s)$HOST($|\s)" ~/.ssh/config  | awk -F ":" '{print $1}'))
    for (( i = 1; i < 5; i++ )); do
      LINE_NUMBER=$((GET_NUMBER + $i))
      echo "Getting hostname.."
      if awk '{if(NR=='$LINE_NUMBER') print $0}' ~/.ssh/config | grep -e "Hostname"; then
        echo "Success!"
        HOST=$(awk '{if(NR=='$LINE_NUMBER') print $0}' ~/.ssh/config | grep -e "Hostname" | sed -e "s/Hostname//" -e "s/ //g")
        break
      fi
    done
    for (( i = 1; i < 5; i++ )); do
      LINE_NUMBER=$((GET_NUMBER + $i))
      echo "Getting user.."
      if awk '{if(NR=='$LINE_NUMBER') print $0}' ~/.ssh/config | grep -e "User"; then
        echo "Success!"
        HOST_USER=$(awk '{if(NR=='$LINE_NUMBER') print $0}' ~/.ssh/config | grep -e "User" | sed -e "s/User//" -e "s/ //g")
        break
      fi
    done
    for (( i = 1; i < 5; i++ )); do
      LINE_NUMBER=$((GET_NUMBER + $i))
      echo "Getting port.."
      if awk '{if(NR=='$LINE_NUMBER') print $0}' ~/.ssh/config | grep -e "Port"; then
        echo "Success!"
        HOST_SSH_PORT=$(awk '{if(NR=='$LINE_NUMBER') print $0}' ~/.ssh/config | grep -e "Port" | sed -e "s/Port//" -e "s/ //g")
        break
      fi
    done
    for (( i = 1; i < 5; i++ )); do
      LINE_NUMBER=$((GET_NUMBER + $i))
      echo "Getting key.."
      if awk '{if(NR=='$LINE_NUMBER') print $0}' ~/.ssh/config | grep -e "IdentityFile"; then
        echo "Success!"
        HOST_SSH_KEY=$(awk '{if(NR=='$LINE_NUMBER') print $0}' ~/.ssh/config | grep -e "IdentityFile" | sed -e "s/IdentityFile//" -e "s/ //g")
        break
      else
        echo "Warning: Key not found!"
      fi
    done
    HOST=$(echo $HOST | sed 's/\-/\\\-/g')
    #HOST_USER=$(echo $HOST_USER | sed 's/\-/\\\-/g')
  fi
  echo
}

function clearHost(){
  unset HOST
  unset HOST_USER
  unset HOST_SSH_PORT
  unset HOST_SSH_KEY
  unset HOST_NAME
  unset HOST_NAME_SSH
}
