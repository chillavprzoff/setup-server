#!/bin/bash

read -p 'Path to Secret Key (default=~/.ssh/id_rsa): ' SECKEY
source ./scripts/general-setup.sh
source ./scripts/add-ssh-port.sh

read -p "Are you want to set hostname? (Y/n): " SET_HOSTNAME
if [[ $SET_HOSTNAME != "n" ]]; then
  source ./scripts/insert-hostname.sh
  APP_NAME=$HOSTNAME_ANSIBLE
fi

if [[ $SECKEY == "" ]]; then
  SECKEY="~/.ssh/id_rsa"
fi

read -p 'Path to your Public Key (default=~/.ssh/id_rsa.pub): ' PUBKEY

if [[ $PUBKEY == "" ]]; then
  PUBKEY=$(<~/.ssh/id_rsa.pub)
else
  PUBKEY=$(<$PUBKEY)
fi

if [[ $PUBKEY == "" ]]; then
  echo "Cannot read public key, plese make sure the path is correct!"
  echo "Exiting.."
  exit
fi

echo "    ansible_ssh_private_key_file: $SECKEY" >>./$INVENTORY_FILE
echo "    PUBLIC_KEY: '$PUBKEY'" >>./$INVENTORY_FILE

source ./scripts/add-more-servers.sh

while true; do
  if [[ $HOSTNAME_ANSIBLE == "" ]]; then
    yq e '.[] | select(.name == "Copy Public Key")' ./yamls/copy-ssh-publickey.yaml >./tmp/copy-publickey.yaml
  else
    yq e '.[] | select(.name == "Copy Public Key Change Hostname")' ./yamls/copy-ssh-publickey.yaml >./tmp/copy-publickey.yaml
  fi
  yq e '[.]' ./tmp/copy-publickey.yaml >./tmp/copy-publickey-array.yaml

  PLAYBOOK_FILE="./tmp/copy-publickey-array.yaml"
  cat $PLAYBOOK_FILE
  break

  #source ./scripts/play-the-playbook.sh
  #source ./scripts/addentry-sshconfig.sh
done
