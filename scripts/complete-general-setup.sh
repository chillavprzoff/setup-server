#!/bin/bash

source ./scripts/general-setup.sh

source ./scripts/add-ssh-port.sh

source ./scripts/add-or-delete-server.sh

source ./scripts/play-the-playbook.sh
