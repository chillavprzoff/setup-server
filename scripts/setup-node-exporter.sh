#!/bin/bash

if [[ ! -d ./tmp/node-exporter ]]; then
  mkdir -p ./tmp/node-exporter
fi
source ./scripts/incomplete-general-setup.sh

HTPASSWD_APP_NAME="node-exporter"
source ./scripts/generate-htpasswd.sh
NODE_EXPORTER_PASSWORD=$HTPASSWD_PASSWORD
NODE_EXPORTER_ENCYRPT_PASSWORD=$HTPASSWD_ENCYRPT_PASSWORD

yq e '.basic_auth_users.prometheus = "'$NODE_EXPORTER_ENCYRPT_PASSWORD'"' ./files/node-exporter/config.yml > ./tmp/node-exporter/config.yml

yq e -i '.servers.vars.node_exporter_password = "'$NODE_EXPORTER_PASSWORD'"' ./$INVENTORY_FILE

# Generate self-sign ssl
echo
source ./scripts/prepare-for-generating-self-sign-cert.sh

yq e -i '.tls_server_config.cert_file = "'$CERT_NAME'-fullchain.pem"' ./tmp/node-exporter/config.yml 
yq e -i '.tls_server_config.key_file = "'$CERT_NAME'-cert-key.pem"' ./tmp/node-exporter/config.yml 
    
source ./scripts/node-exporter-port.sh

yq e -i '.servers.vars.node_exporter_port = "'$NODE_EXPORTER_PORT'"' ./$INVENTORY_FILE
sed "s/address=:.*/address=:$NODE_EXPORTER_PORT/" ./files/node-exporter/node-exporter.service > ./tmp/node-exporter/node-exporter.service
if [[ ! -d ./tmp/prometheus-grafana ]]; then
  mkdir -p ./tmp/prometheus-grafana
fi
yq e '.services.node-exporter.ports[0] = "'$NODE_EXPORTER_PORT':9100"' ./files/docker-compose/prometheus-grafana/docker-compose.yml > ./tmp/prometheus-grafana/docker-compose.yml

if source ./scripts/play-the-playbook.sh; then

  if [[ -f ./tmp/node-exporter/node-exporter-targets.yml ]]; then
    rm -rf ./tmp/node-exporter/node-exporter-targets.yml
  fi

  source ./scripts/node-exporter-targets.sh

  cat ./tmp/node-exporter/node-exporter-targets.yml
  
  echo -e "You can see output file of node exporter targets at: ./tmp/node-exporter-targets.yml\ncopy to your prometheus 'node-exporter' job!"
  echo ""
  echo "Node-exporter Username: prometheus"
  echo "Node-exporter Password: $NODE_EXPORTER_PASSWORD"
  echo ""

  read -p "Are you want to setup prometheus and grafana? (Y/n): " SETUP_PROMETHEUS_GRAFANA
  if [[ $SETUP_PROMETHEUS_GRAFANA != "n" ]]; then
    NAME_HOSTS="servers"
    source ./scripts/remove-all-host.sh
    IPSERVER=""
    : > ./tmp/ip${NAME_HOSTS}
    source ./scripts/add-ip-server.sh
    addIpServer
    source ./scripts/subtitute-ip-server.sh
    source ./scripts/add-or-delete-server.sh
    source ./scripts/prometheus-grafana-docker.sh
    PLAYBOOK_FILE="yamls/setup-prometheus-grafana-docker.yaml"
    if source ./scripts/play-the-playbook.sh; then
      echo "Prometheus Username: admin"
      echo "Prometheus Password: $PROMETHEUS_PASSWORD"
    fi
    
  fi

fi
if [[ -f ./tmp/prometheus-grafana/docker-compose.yml ]]; then
  rm -rf ./tmp/prometheus-grafana/docker-compose.yml
fi
