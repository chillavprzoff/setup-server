#!/bin/bash

source ./scripts/add-bastion.sh

function setupBastion() {

read -p 'App Name : ' APP_NAME

addBastion

source ./scripts/ask-for-copy-pubkey.sh

echo "[bastion]
$IPBASTION

[bastion:vars]
ansible_user=$USER_BASTION
ansible_ssh_private_key_file=$BASTIONKEY" > ./hosts/bastion

echo "APP_NAME: "${APP_NAME}"
PUBLIC_KEY: "$PUBKEY" 
SSHPORT_BASTION: $SSHPORT_BASTION" > ./envs/bastion.yaml

while true; do
  if [[ $SSHPORT_BASTION != "" ]] || [[ $SSHPORT_BASTION != "n" ]] || [[ $SSHPORT_BASTION != "N" ]]; then
    yq e '.[] | select(.name == "Setup Bastion Change Port")' ./yamls/setup-bastion.yaml > ./tmp/setup-bastion.yaml
  else
    yq e '.[] | select(.name == "Setup Bastion")' ./yamls/setup-bastion.yaml > ./tmp/setup-bastion.yaml
  fi

  ansible-playbook -i ./hosts/bastion ./tmp/setup-bastion.yaml

  read -p "Create entry in ssh config for the bastion host? (Y/n): " SSH_CONFIG
  echo "" >> ~/.ssh/config
  if [[ $SSH_CONFIG != "n" ]]; then
    if [[ $SSHPORT_BASTION != "" ]] || [[ $SSHPORT_BASTION != "n" ]] || [[ $SSHPORT_BASTION != "N" ]]; then
    echo "Host $APP_NAME.host
  Hostname $IPBASTION
  Port $SSHPORT_BASTION
  User $(if [[ $USER_BASTION != "" ]]; then echo $USER_BASTION; else echo 'ec2-user'; fi)" >> ~/.ssh/config
    else
    echo "Host $APP_NAME.host
  Hostname $IPBASTION 
  Port 22
  User $(if [[ $USER_BASTION != "" ]]; then echo $USER_BASTION; else echo 'ec2-user'; fi)" >> ~/.ssh/config

    fi

    echo ""
    echo "------------------"
    echo "ssh $APP_NAME.host"
    echo "------------------"
    echo ""

  else
    if [[ $USER_BASTION != "" ]]; then
      echo "ssh $USER_BASTION@$IPBASTION $(if [[ $SSHPORT_BASTION != "" ]]; then echo "'-p $SSHPORT_BASTION'"; else echo ''; fi)";
    else
      echo "ssh ec2-user@$IPBASTION $(if [[ $SSHPORT_BASTION != "" ]]; then echo "'-p $SSHPORT_BASTION'"; else echo ''; fi)";
    fi

  fi
  
  read -p 'retry? (Y/n): ' RETRY
  if [[ $RETRY == n ]]; then
    break
  fi

done

}
