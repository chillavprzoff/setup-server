#!/bin/bash

while [[ true ]]; do
	SERVER_HOSTS=($(tr '\n' ' ' <./tmp/ip${NAME_HOSTS}-uniq))
	read -p "Are you want to delete a target server? (y/n): " DELETE_SERVER
	if [[ $SERVER_HOSTS == "" ]]; then
		echo "There is no ip found please add an ip first.."
		break
	else
		SERVER_HOSTS+=("cancel")
	fi
	if [[ $DELETE_SERVER != "y" ]] && [[ $DELETE_SERVER != "n" ]]; then
		echo "you must answer 'y' or 'n'!"
	elif [[ $DELETE_SERVER == "y" ]]; then
		PS3='Select a target server you want to delete: '
		select opt in "${SERVER_HOSTS[@]}"; do
			if [[ $opt == "cancel" ]]; then
				break
			fi
			GET_UNIQ_IP=$(grep -v "$opt" ./tmp/ip${NAME_HOSTS}-uniq)
			if [[ $GET_UNIQ_IP == "" ]]; then
				if cat ./tmp/ip${NAME_HOSTS}-uniq | grep -q "$opt"; then
					echo "" >./tmp/ip${NAME_HOSTS}-uniq
				fi
			else
				grep -v "$opt" ./tmp/ip${NAME_HOSTS}-uniq >./tmp/tmpip${NAME_HOSTS} && mv ./tmp/tmpip${NAME_HOSTS} ./tmp/ip${NAME_HOSTS}-uniq
			fi
			yq e -i 'del(.'$NAME_HOSTS'.hosts."'$opt'")' ./$INVENTORY_FILE
			break
		done
	elif [[ $DELETE_SERVER == "n" ]]; then
		break
	fi
done
cp ./tmp/ip${NAME_HOSTS}-uniq ./tmp/ip${NAME_HOSTS}
