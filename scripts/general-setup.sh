#!/bin/bash

if [[ $NAME_HOSTS == "" ]]; then
  NAME_HOSTS="servers"
fi

if [[ ! -f ./$INVENTORY_FILE ]]; then
  source ./scripts/add-ip-server.sh
  source ./scripts/add-bastion.sh
  askBastion

  SEARCH_HOST="Server Host"
  read -p "Search $SEARCH_HOST in ~/.ssh/config? (Y/n): " IS_SEARCH_HOST
  if [[ $IS_SEARCH_HOST != "n" ]] && [[ $IS_SEARCH_HOST != "N" ]]; then
    searchHost
    setHost
    IPSERVER=$HOST
    SSH_PORT=$HOST_SSH_PORT
    USER_ANSIBLE=$HOST_USER
    PRIVATEKEY=$HOST_SSH_KEY
    echo "Server Host IP: $IPSERVER"
    echo "Server Host User: $USER_ANSIBLE"
    echo "Server Host SSH Port: $SSH_PORT"
    echo "Server Host Private Key: $PRIVATEKEY"
    echo "Success get server host detail!"
    clearHost
    echo
  else
    if [[ $IPSERVER == "" ]]; then
      addIpServer
    fi

    source ./scripts/add-ssh-port.sh
    if [[ $USER_ANSIBLE == "" ]]; then
      read -p 'User [devops]: ' USER_ANSIBLE
      if [[ $USER_ANSIBLE == "" ]]; then
        USER_ANSIBLE="devops"
      fi
    fi
  fi

  function createTmpIpFile() {
    echo $IPSERVER >>./tmp/ip${NAME_HOSTS}
    sort ./tmp/ip${NAME_HOSTS} | uniq >./tmp/ip${NAME_HOSTS}-uniq
  }

  echo "$NAME_HOSTS:
  hosts:
    $IPSERVER:
  vars:
    ansible_user: $USER_ANSIBLE
    ansible_ssh_common_args: '-o StrictHostKeyChecking=no'" >./$INVENTORY_FILE
  createTmpIpFile
  if [[ $BASTION != "n" ]] && [[ $BASTION != "N" ]]; then
    if [[ $BASTIONKEY == "" ]]; then
      if [[ -f ~/.ssh/id_rsa ]]; then
        BASTIONKEY="~/.ssh/id_rsa"
      else
        echo "Cannot find private key to connect with bastion host!!"
        exit
      fi
    fi
    #BASTIONKEY=$(echo $BASTIONKEY | sed 's/\//\\\//g')
    yq e -i '.'${NAME_HOSTS}'.vars.ansible_ssh_common_args = "-o StrictHostKeyChecking=no -o ProxyCommand=\"ssh -p '$SSHPORT_BASTION' -W %h:%p -q '$USER_BASTION'@'$IPBASTION' -i '$BASTIONKEY'\""' ./$INVENTORY_FILE
    if [[ $NAME_HOSTS == "clients" ]]; then
      if [[ -f ./tmp/ip$NAME_HOSTS-uniq ]]; then
        CLIENT_HOSTS=($(tr '\n' ' ' <./tmp/ip${NAME_HOSTS}-uniq))
        CLIENT_LEN=${#CLIENT_HOSTS[@]}
        if [[ $CLIENT_LEN -ge 1 ]]; then
          for s in ${CLIENT_HOSTS[@]}; do
            yq e -i '.'${NAME_HOSTS}'.hosts["'$s'"] = null' ./$INVENTORY_FILE
          done
        fi
      fi
      yq e -i '.'${NAME_HOSTS}'.hosts["'$s'"] = null' ./$INVENTORY_FILE
    fi
  fi
  if [[ $PRIVATEKEY != "" ]]; then
    echo "    ansible_ssh_private_key_file: '$PRIVATEKEY'" >>./$INVENTORY_FILE
  fi
  if [[ $SSH_PORT != "" ]]; then
    echo "    ansible_ssh_port: $SSH_PORT" >>./$INVENTORY_FILE
  else
    echo "    ansible_ssh_port: 22" >>./$INVENTORY_FILE
  fi
else
  USER_ANSIBLE=$(yq e '.'${NAME_HOSTS}'.vars.ansible_user' ./$INVENTORY_FILE)
  SSH_PASSWORD=$(yq e '.'${NAME_HOSTS}'.vars.ansible_ssh_pass' ./$INVENTORY_FILE)
fi

PRIVATEKEY=$(yq e '.'${NAME_HOSTS}'.vars.ansible_ssh_private_key_file' ./$INVENTORY_FILE)
if [[ $PRIVATEKEY == "" ]] && [[ $PRIVATEKEY == null ]]; then
  source ../scripts/ask-for-access-server.sh
fi
