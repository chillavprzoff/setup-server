#!/bin/bash
SERVER_HOSTS=($(tr '\n' ' ' <./tmp/ip${NAME_HOSTS}-uniq))
for s in ${SERVER_HOSTS[@]}; do
NODE_EXPORTER_HOSTNAME=$(ssh -p $SSH_PORT $USER_ANSIBLE@$s hostname -s)
echo "- targets:
  - $s:$NODE_EXPORTER_PORT
  labels:
    node: $NODE_EXPORTER_HOSTNAME
    job: node-exporter" >> ./tmp/node-exporter/node-exporter-targets.yml
echo "" >> ./tmp/node-exporter/node-exporter-targets.yml
done
