#!/bin/bash

read -p "Are you want to change default node-exporter port? [9100] (y/N): " NODE_EXPORTER_CHANGE_DEFAULT_PORT
if [[ $NODE_EXPORTER_CHANGE_DEFAULT_PORT == "y" ]]; then
  while true; do
    read -p "Node-exporter port: " NODE_EXPORTER_PORT
    if [[ $NODE_EXPORTER_PORT == "" ]]; then
      echo "Please enter node-exporter port!!!"
    else
      break
    fi
  done
else
  NODE_EXPORTER_PORT=9100
fi
