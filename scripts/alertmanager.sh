#!/bin/bash
 
source ./scripts/incomplete-general-setup.sh

RECEIVERS=("telegram" "discord")

PS3='Please choose receivers: '
select opt in "${RECEIVERS[@]}"
do
  RECEIVER=$opt
  break
done

if [[ $RECEIVER == "telegram" ]]; then
  TELEGRAM_BOT_TOKEN=$(yq e '.'$NAME_HOSTS'.vars.telegram_bot_token' $INVENTORY_FILE)
  if [[ $TELEGRAM_BOT_TOKEN == "" ]] || [[ $TELEGRAM_BOT_TOKEN == null ]]; then
    read -p "Telegram bot token: " TELEGRAM_BOT_TOKEN
    yq e -i '.'$NAME_HOSTS'.vars.telegram_bot_token = "'$TELEGRAM_BOT_TOKEN'"' $INVENTORY_FILE
  fi
  TELEGRAM_CHAT_ID=$(yq e '.'$NAME_HOSTS'.vars.telegram_bot_chatid' $INVENTORY_FILE)
  if [[ $TELEGRAM_CHAT_ID == "" ]] || [[ $TELEGRAM_CHAT_ID == null ]]; then
    read -p "Telegram chat id: " TELEGRAM_CHAT_ID
    yq e -i '.'$NAME_HOSTS'.vars.telegram_bot_chatid = "'$TELEGRAM_CHAT_ID'"' $INVENTORY_FILE
  fi

  # this script for prometheus.prometheus.alertmanager but right now cannot be used.
  #yq e '.name = "'$RECEIVER'"' ./files/prometheus/alertmanager/telegram-receivers.yaml > ./tmp/telegram-receivers.yaml
  #yq e -i '.telegram_configs[0].bot_token = "'$TELEGRAM_BOT_TOKEN'"' ./tmp/telegram-receivers.yaml
  #yq e -i '.telegram_configs[0].chat_id = "'$TELEGRAM_CHAT_ID'"' ./tmp/telegram-receivers.yaml
  #yq e '.[0].vars.alertmanager_receivers += load("./tmp/telegram-receivers.yaml")' ./playbooks/alertmanager.yaml > ./tmp/alertmanager.yaml
  #yq e -i '.[0].vars.alertmanager_route.receiver = "'$RECEIVER'"' ./tmp/alertmanager.yaml
elif [[ $RECEIVER == "discord" ]]; then
  echo "Sorry the prometheus.prometheus.alertmanager roles cannot be use right now so the discord receiver cannot be use, use telegram receiver instead."
  #read -p "Discord webhook url: " DISCORD_WEBHOOK_URL
  #yq e '.name = "'$RECEIVER'"' ./files/prometheus/alertmanager/telegram-receivers.yaml > ./tmp/telegram-receivers.yaml
  #yq e -i '.discord_configs[0].webhook_url = "'$DISCORD_WEBHOOK_URL'"' ./tmp/discord-receivers.yaml
  #yq e '.[0].vars.alertmanager_receivers += load("./tmp/discord-receivers.yaml")' ./playbooks/alertmanager.yaml > ./tmp/alertmanager.yaml
  #yq e -i '.[0].vars.alertmanager_route.receiver = "'$RECEIVER'"' ./tmp/alertmanager.yaml
  exit
fi

# this script for prometheus.prometheus.alertmanager but right now cannot be used.
#PLAYBOOK_FILE="./tmp/alertmanager.yaml"

source ./scripts/play-the-playbook.sh
