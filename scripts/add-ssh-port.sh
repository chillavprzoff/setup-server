#!/bin/bash

if [[ $SSH_PORT == "" ]]; then
	if [[ -f $INVENTORY_FILE ]]; then
		IS_SSH_PORT=$(yq e '.'$NAME_HOSTS'.vars.ansible_ssh_port' $INVENTORY_FILE)
	fi
	if [[ $IS_SSH_PORT == "" ]] || [[ $IS_SSH_PORT == "null" ]]; then
		read -p "Connect with non-general ssh port (22)? (Y/n): " CHANGE_SSH
		if [[ $CHANGE_SSH != "n" ]]; then
			if [[ $CHANGE_SSH != "N" ]]; then
				read -p 'SSH Port : ' SSH_PORT
				echo "    ansible_ssh_port: $SSH_PORT" >>./$INVENTORY_FILE
			else
				SSH_PORT="22"
			fi
		else
			SSH_PORT="22"
		fi
	fi
fi
