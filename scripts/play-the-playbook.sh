#!/bin/bash

while true; do
  if ! ansible-playbook -i ./$INVENTORY_FILE ./$PLAYBOOK_FILE; then
    read -p 'retry? (Y/n): ' RETRY
    if [[ $RETRY == "n" ]]; then
      return 1
      break
    fi
  else
    break
  fi
done
