#!/bin/bash

source ./scripts/general-setup.sh
source ./scripts/add-ssh-port.sh
source ./scripts/ask-for-copy-pubkey.sh
source ./scripts/add-more-servers.sh
if source ./scripts/play-the-playbook.sh; then
  USER_ANSIBLE="devops"
  source ./scripts/addentry-sshconfig.sh
fi
