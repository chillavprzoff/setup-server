#!/bin/bash

source ./scripts/incomplete-general-setup.sh

cp ./scripts/create-grafana-dashboard.sh ./tmp/create-grafana-dashboard.sh
read -p "Grafana URL [http://localhost:3000]: " GRAFANA_URL
if [[ $GRAFANA_URL == "" ]]; then
  GRAFANA_URL="http://localhost:3000"
fi
read -p "Grafana Username [admin]: " GRAFANA_USERNAME
if [[ $GRAFANA_USERNAME == "" ]]; then
  GRAFANA_USERNAME="admin"
fi
read -p "Grafana Password [admin]: " GRAFANA_PASSWORD
if [[ $GRAFANA_PASSWORD == "" ]]; then
  GRAFANA_PASSWORD="admin"
fi
while [[ true ]]; do
  read -p "Enter grafana dashboard id to import the grafana dashboard, seperate with space if there is multiple id: " GRAFANA_DASHBOARD_IMPORT
  if [[ $GRAFANA_DASHBOARD_IMPORT != "" ]]; then
    break
  fi
done
GRAFANA_URL=$(echo $GRAFANA_URL | sed "s/\//\\\\\\//g")
sed -i "s/GRAFANA_URL/$GRAFANA_URL/" ./tmp/create-grafana-dashboard.sh
sed -i "s/GRAFANA_USERNAME/$GRAFANA_USERNAME/" ./tmp/create-grafana-dashboard.sh
sed -i "s/GRAFANA_PASSWORD/$GRAFANA_PASSWORD/" ./tmp/create-grafana-dashboard.sh
sed -i "s/GRAFANA_DASHBOARD_IMPORT/$GRAFANA_DASHBOARD_IMPORT/" ./tmp/create-grafana-dashboard.sh

source ./scripts/play-the-playbook.sh
