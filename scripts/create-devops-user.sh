#!/bin/bash
useradd -m -s /bin/bash devops
usermod -aG sudo devops
usermod -aG adm devops
usermod -aG dialout devops
usermod -aG cdrom devops
usermod -aG floppy devops
usermod -aG audio devops
usermod -aG dip devops
usermod -aG video devops
usermod -aG plugdev devops
usermod -aG netdev devops
usermod -aG lxd devops

# OLD
#cp /etc/sudoers.d/90-cloud-init-users /etc/sudoers.d/91-devops-users
#sed -i 's/ubuntu/devops/g' /etc/sudoers.d/91-devops-users
#cp /home/ubuntu/.ssh /home/devops/ -r
#chown -R devops:devops /home/devops/.ssh
# OLD

echo "devops ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/devops-user
mkdir /home/devops/.ssh
if [[ ! -f /home/devops/.ssh/id_rsa ]]; then
  ssh-keygen -t rsa -P "" -f /home/devops/.ssh/id_rsa -C "devops"
fi
chown -R devops:devops /home/devops/.ssh
