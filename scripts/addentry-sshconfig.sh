#!/bin/bash
read -p "Create entry in ssh config for the host? (Y/n): " SSH_CONFIG

if [[ $SSH_CONFIG != "n" ]]; then
	read -p "Write to ~/.ssh/config ? (y/N): " WRITE_TO_SSH_CONFIG
	if [[ $APP_NAME == "" ]]; then
		read -p 'App Name : ' APP_NAME
	fi

	if [[ $WRITE_TO_SSH_CONFIG != "y" ]] && [[ $WRITE_TO_SSH_CONFIG != "Y" ]]; then
		SERVER_HOSTS=($(tr '\n' ' ' <./tmp/ip$NAME_HOSTS-uniq))
		for s in ${SERVER_HOSTS[@]}; do
			if [[ $SSH_PORT != "" ]] || [[ $SSH_PORT != "n" ]] || [[ $SSH_PORT != "N" ]]; then
				echo "Host $APP_NAME
  Hostname $s
  Port $SSH_PORT
  User $(if [[ $USER_ANSIBLE != "" ]]; then echo $USER_ANSIBLE; else echo 'ec2-user'; fi)"
			else
        echo "Host $APP_NAME
  Hostname $s
  Port 22
  User $(if [[ $USER_ANSIBLE != "" ]]; then echo $USER_ANSIBLE; else echo 'ec2-user'; fi)"
			fi
		done
	else
		SERVER_HOSTS=($(tr '\n' ' ' <./tmp/ip$NAME_HOSTS-uniq))
		for s in ${SERVER_HOSTS[@]}; do
			echo "" >>~/.ssh/config

			if [[ $SSH_PORT != "" ]] || [[ $SSH_PORT != "n" ]] || [[ $SSH_PORT != "N" ]]; then
				echo "Host $APP_NAME
  Hostname $s
  Port $SSH_PORT
  User $(if [[ $USER_ANSIBLE != "" ]]; then echo $USER_ANSIBLE; else echo 'ec2-user'; fi)" >>~/.ssh/config
			else
				echo "Host $APP_NAME
  Hostname $s
  Port 22
  User $(if [[ $USER_ANSIBLE != "" ]]; then echo $USER_ANSIBLE; else echo 'ec2-user'; fi)" >>~/.ssh/config
			fi

			echo ""
			echo "------------------"
			echo "ssh $APP_NAME"
			echo "------------------"
			echo ""
		done
	fi

else
	SERVER_HOSTS=($(tr '\n' ' ' <./tmp/ip$NAME_HOSTS-uniq))
	for s in ${SERVER_HOSTS[@]}; do
		if [[ $USER_ANSIBLE != "" ]]; then
			echo ""
			echo "------------------"
			echo "ssh $USER_ANSIBLE@$s $(if [[ $SSH_PORT != "" && $SSH_PORT != "22" ]]; then echo "'-p $SSH_PORT'"; else echo ''; fi)"
			echo "------------------"
			echo ""
		else
			echo ""
			echo "------------------"
			echo "ssh ec2-user@$s $(if [[ $SSH_PORT != "" && $SSH_PORT != "22" ]]; then echo "'-p $SSH_PORT'"; else echo ''; fi)"
			echo "------------------"
			echo ""
		fi
	done

fi
