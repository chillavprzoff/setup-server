#!/bin/bash

if [[ $NAME_HOSTS == "" ]]; then
  NAME_HOSTS="servers"
fi

source ./scripts/add-ip-server.sh

while [[ true ]]; do
	read -p "Add More Server? (Y/n): " ADDMORE
  SERVER_HOSTS=($(tr '\n' ' ' <./tmp/ip${NAME_HOSTS}-uniq))
	if [[ $ADDMORE == "n" ]] || [[ $ADDMORE == "N" ]]; then
    if [[ $SERVER_HOSTS == "" ]]; then
      echo "Cannot continue without at least one target ip server.. or ctrl + c to exit the script"
    else
		  break
    fi
	else
		ADDMORE_SERVER=true
		read -p "Single IP or Multiple(Bulk)? Answer S/s|single M/m|multiple: " ADD_SERVER_BULK_OR_SINGLE
		if [[ $ADD_SERVER_BULK_OR_SINGLE == "S" ]] || [[ $ADD_SERVER_BULK_OR_SINGLE == "s" ]]; then
			addIpServer
			yq e -i '.'${NAME_HOSTS}'.hosts["'$IPSERVER'"] = null' ./$INVENTORY_FILE
		elif [[ $ADD_SERVER_BULK_OR_SINGLE == "M" ]] || [[ $ADD_SERVER_BULK_OR_SINGLE == "m" ]]; then
			echo "# !!! Remove this line and Paste the IP of ${NAME_HOSTS} you want to add !!!" >./tmp/hosts
			vi ./tmp/hosts
			cat ./tmp/hosts >>./tmp/ip${NAME_HOSTS}
			sort ./tmp/ip${NAME_HOSTS} | uniq >./tmp/ip${NAME_HOSTS}-uniq
			SERVER_HOSTS=($(tr '\n' ' ' <./tmp/ip${NAME_HOSTS}-uniq))
			for s in ${SERVER_HOSTS[@]}; do
				yq e -i '.'${NAME_HOSTS}'.hosts["'$s'"] = null' ./$INVENTORY_FILE
			done
		fi
	fi
done
