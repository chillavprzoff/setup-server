#!/bin/bash

source ./scripts/incomplete-general-setup.sh

###CUSTOM

ANSIBLE_USER=$(yq e '.'${NAME_HOSTS}'.vars.ansible_user' ./$INVENTORY_FILE)

source ./scripts/add-variable-to-inventory.sh
if [[ $ANSIBLE_USER != "" ]]; then
  read -p "Use [$ANSIBLE_USER] as a Docker User? (Y/n): " USE_ANSIBLE_USER_AS_DOCKER_USER
  if [[ $USE_ANSIBLE_USER_AS_DOCKER_USER != "n" ]] && [[ $USE_ANSIBLE_USER_AS_DOCKER_USER != "N" ]]; then
    yq e -i '.'$NAME_HOSTS'.vars.DOCKER_USER = "'$ANSIBLE_USER'"' $INVENTORY_FILE
  else
    addVariable "Docker User" DOCKER_USER
  fi
else
  addVariable "Docker User" DOCKER_USER
fi

###CUSTOM

ansible-galaxy install geerlingguy.docker
source ./scripts/play-the-playbook.sh
