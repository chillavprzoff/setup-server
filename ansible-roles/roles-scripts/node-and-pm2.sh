#!/bin/bash
source ./scripts/incomplete-general-setup.sh

read -p "Please enter nodejs_version: " NODE_JS_VERSION
yq e '(.[] | select(.hosts == "'$NAME_HOSTS'")).roles[0].vars.nodejs_version = "'$NODE_JS_VERSION'"' ./playbooks/node-and-pm2.yaml > ./tmp/node-and-pm2.yaml

PLAYBOOK_FILE="./tmp/node-and-pm2.yaml"
source ./scripts/play-the-playbook.sh
