#!/bin/bash

NAME_HOSTS="clients"
if [[ ! -f tmp/$NAME_HOSTS-uniq ]]; then
  touch ./tmp/$NAME_HOSTS-uniq
fi

source ./scripts/incomplete-general-setup.sh

read -p "IP NFS server: " IP_NFS_SERVER
read -p "NFS server mount path(fullpath): " NFS_SERVER_PATH
read -p "NFS client mount path(fullpath): " NFS_CLIENT_PATH
read -p "NFS mount options [auto,nofail,noatime,nolock,intr,tcp,actimeo=1800]: " NFS_MOUNT_OPTIONS
cp ./playbooks/nfs-client.yaml ./tmp/nfs-client.yaml
yq e -i '(.[] | select(.hosts == "clients")).vars.nfs_shares.[0].remote_path = "'$IP_NFS_SERVER':'$NFS_SERVER_PATH'"' ./tmp/nfs-client.yaml
yq e -i '(.[] | select(.hosts == "clients")).vars.nfs_shares.[0].mnt_path = "'$NFS_CLIENT_PATH'"' ./tmp/nfs-client.yaml
yq e -i '(.[] | select(.hosts == "clients")).vars.nfs_mount_opts = "'$NFS_MOUNT_OPTIONS'"' ./tmp/nfs-client.yaml

PLAYBOOK_FILE="./tmp/nfs-client.yaml"
source ./scripts/play-the-playbook.sh
