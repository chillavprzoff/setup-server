#!/bin/bash

source ./scripts/incomplete-general-setup.sh

read -p "Create postgres regular user, username: " POSTGRES_USER_USERNAME
read -p "Generate random password for [$POSTGRES_USER_USERNAME]? (Y/n): " POSTGRES_GEN_PASS
if [[ $POSTGRES_GEN_PASS != "n" ]]; then
  POSTGRES_USER_PASSWORD=$(cat /dev/urandom | LC_ALL=C tr -dc 'a-zA-Z0-9' | fold -w 50 | head -n 1)
else
  read -p "Enter password for [$POSTGRES_USER_USERNAME]: " POSTGRES_USER_PASSWORD
fi
read -p "Create database for [$POSTGRES_USER_USERNAME], the database name same with username? (Y/n): " POSTGRES_DBNAME_SAME_WITH_USERNAME
if [[ $POSTGRES_DBNAME_SAME_WITH_USERNAME == "n" ]]; then
  read -p "Database Name: "
else
  POSTGRES_USER_DATABASE=$POSTGRES_USER_USERNAME
fi

yq e '.[0].roles.[0].vars.postgresql_users[0].name = "'$POSTGRES_USER_USERNAME'"' ./$PLAYBOOK_FILE > ./tmp/$(echo $PLAYBOOK_FILE | awk -F "/" '{print $3}')
yq e -i '.[0].roles.[0].vars.postgresql_users[0].password = "'$POSTGRES_USER_PASSWORD'"' ./tmp/$(echo $PLAYBOOK_FILE | awk -F "/" '{print $3}')
yq e -i '.[0].roles.[0].vars.postgresql_databases[0].name = "'$POSTGRES_USER_DATABASE'"' ./tmp/$(echo $PLAYBOOK_FILE | awk -F "/" '{print $3}')
yq e -i '.[0].roles.[0].vars.postgresql_databases[0].owner = "'$POSTGRES_USER_USERNAME'"' ./tmp/$(echo $PLAYBOOK_FILE | awk -F "/" '{print $3}')

echo ""
echo "Username: $POSTGRES_USER_USERNAME"
echo "Password: $POSTGRES_USER_PASSWORD"
echo "DatabaseName: $POSTGRES_USER_DATABASE"
echo ""

ansible-galaxy install geerlingguy.postgresql
PLAYBOOK_FILE="./tmp/$(echo $PLAYBOOK_FILE | awk -F "/" '{print $3}')"
source ./scripts/play-the-playbook.sh
