#!/bin/bash

source ./scripts/incomplete-general-setup.sh

IP_NFS_SERVER=$IPSERVER

read -p "Add client server? (Y/n): " NFS_ADD_CLIENT_SERVER
if [[ $NFS_ADD_CLIENT_SERVER != "n" ]] || [[ $NFS_ADD_CLIENT_SERVER != "N" ]]; then
	# add clients server to nfs client
	IPSERVER=""
	NAME_HOSTS="clients"
	if [[ -f ./tmp/ip${NAME_HOSTS} ]]; then
		rm -rf ./tmp/ip${NAME_HOSTS}
	fi
	if [[ -f ./tmp/ip${NAME_HOSTS}-uniq ]]; then
		rm -rf ./tmp/ip${NAME_HOSTS}-uniq
	fi
	ADDMORE_SERVER=true
	echo -e "${NAME_HOSTS}:\n  hosts:" >>./$INVENTORY_FILE
	source ./scripts/add-ip-server.sh
	addIpServer
	yq e -i '.'${NAME_HOSTS}'.hosts["'$IPSERVER'"] = null' ./$INVENTORY_FILE
	source ./scripts/add-more-servers.sh

	read -p "NFS server path(fullpath): " NFS_SERVER_PATH

	cp ./nfs-server/defaults/main.yml ./nfs-server/vars/main.yml
	CLIENT_HOSTS=($(tr '\n' ' ' <./tmp/ip${NAME_HOSTS}-uniq))
	CLIENT_LEN=0
	for client in ${CLIENT_HOSTS[@]}; do
		yq e -i '.nfs_server_exports.['$CLIENT_LEN'].export = null' nfs-server/vars/main.yml
		yq e -i '.nfs_server_exports.['$CLIENT_LEN'].access.[0].hostname = "'$client'/32"' nfs-server/vars/main.yml
		yq e -i '.nfs_server_exports.['$CLIENT_LEN'].access.[0].options[0] = "rw"' nfs-server/vars/main.yml
		yq e -i '.nfs_server_exports.['$CLIENT_LEN'].access.[0].options[1] = "sync"' nfs-server/vars/main.yml
		yq e -i '.nfs_server_exports.['$CLIENT_LEN'].access.[0].options[2] = "no_root_squash"' nfs-server/vars/main.yml
		yq e -i '.nfs_server_exports.['$CLIENT_LEN'].mode = "u=rwx,g=rwx,o=rwx"' nfs-server/vars/main.yml
		yq e -i '.nfs_server_exports.['$CLIENT_LEN'].path = "'$NFS_SERVER_PATH'"' nfs-server/vars/main.yml
		CLIENT_LEN=$(($CLIENT_LEN + 1))
	done
fi

source ./scripts/play-the-playbook.sh

if [[ $NFS_ADD_CLIENT_SERVER != "n" ]] || [[ $NFS_ADD_CLIENT_SERVER != "N" ]]; then
	read -p "Setup client server? (Y/n): " NFS_SETUP_CLIENT_SERVER
	if [[ $NFS_SETUP_CLIENT_SERVER != "n" ]] || [[ $NFS_SETUP_CLIENT_SERVER != "N" ]]; then
		read -p "Use the same credentials as nfs server? (Y/n): " NFS_SAME_CLIENT_CREDS
		if [[ $NFS_SAME_CLIENT_CREDS != "n" ]] || [[ $NFS_SAME_CLIENT_CREDS != "N" ]]; then
			yq e -i '.clients.vars += .servers.vars' ./$INVENTORY_FILE
		else
			./script/general-setup.sh
		fi

		read -p "NFS client mount path(fullpath): " NFS_CLIENT_PATH
		cp ./nfs-client/defaults/main.yml ./nfs-client/vars/main.yml
		yq e -i '.nfs_shares.[0].mnt_path = "'$NFS_CLIENT_PATH'"' nfs-client/vars/main.yml
		yq e -i '.nfs_shares.[0].remote_path = "'$IP_NFS_SERVER':'$NFS_SERVER_PATH'"' nfs-client/vars/main.yml
	fi

	PLAYBOOK_FILE="playbooks/nfs-client.yaml"
	source ./scripts/play-the-playbook.sh

fi

cd ./nfs-server && git restore vars/main.yml
cd - >/dev/null 2>&1
cd ./nfs-client && git restore vars/main.yml
cd - >/dev/null 2>&1
