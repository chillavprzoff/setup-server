#!/bin/bash

source ./scripts/incomplete-general-setup.sh
source ./scripts/add-variable.sh

VARIABLE_NAME_TEXT="Domain"
VARIABLE_NAME_IN_INVENTORY_FILE="domain_name"
addVariable

source ./scripts/play-the-playbook.sh
