#!/bin/bash

source ./scripts/incomplete-general-setup.sh
source ./scripts/add-variable-to-inventory.sh

addVariable "Domain Name" domain_name

NGINX_PROXY_URL=$(yq e '.'$NAME_HOSTS'.vars.proxy_url' $INVENTORY_FILE)
if [[ $NGINX_PROXY_URL != "" ]] && [[ $NGINX_PROXY_URL != "null" ]]; then
  read -p "Found proxy url '$NGINX_PROXY_URL', are you want to change the proxy url? (Y/n): " NGINX_PROXY_URL_CHANGE
  if [[ $NGINX_PROXY_URL_CHANGE != "n" ]] && [[ $NGINX_PROXY_URL_CHANGE != "N" ]]; then
    addVariable "Proxy URL" proxy_url
  fi
else
  addVariable "Proxy URL" proxy_url
fi

source ./scripts/play-the-playbook.sh
