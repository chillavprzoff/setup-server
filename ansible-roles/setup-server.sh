#!/bin/bash

if [[ $SETUP_SCRIPT_ENABLED != true ]]; then
  echo "Please run the setup script from root folder!"
  exit 1
fi

cd "$(dirname "$0")"

if [[ -f .tmpenv ]]; then
	rm -rf .tmpenv
fi

PLAYBOOKS=($(ls ./playbooks))
PLAYBOOKS+=("back to previous option")

while [[ true ]]; do

	PS3='Please choose playbook you want to play: '
	select opt in "${PLAYBOOKS[@]}"; do
    if [[ $opt == "back to previous option" ]]; then
      exit
    fi
		PLAYBOOK_FILE="./playbooks/$opt"
		PLAYBOOK_SCRIPT="$(echo $opt | sed "s/\.yaml/\.sh/")"
    if [[ -f ./roles-scripts/$PLAYBOOK_SCRIPT ]]; then
			source ./roles-scripts/$PLAYBOOK_SCRIPT
		elif [[ -f ./scripts/$PLAYBOOK_SCRIPT ]]; then
			source ./scripts/$PLAYBOOK_SCRIPT
		else
			source ./scripts/complete-general-setup.sh
		fi
		break
	done

done
