# Setup Server

Automatic setup server with interactive shell script.

## Dependencies

- ansible
    - [mac and linux](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html): `python3 -m pip install --user ansible`
- yq
    - [mac](https://formulae.brew.sh/formula/yq): `brew install yq`
    - ubuntu: `apt install yq` 
    > required yq v4
- [geerlingguy postgresql role](https://github.com/geerlingguy/ansible-role-postgresql) (optional: if you want to use postgresql ansible role by geerlingguy)
  ```bash
  ansible-galaxy install geerlingguy.postgresql
  ```
- [geerlingguy postgresql role](https://github.com/geerlingguy/ansible-role-docker) (optional: if you want to use docker ansible role by geerlingguy)
  ```bash
  ansible-galaxy install geerlingguy.docker
  ```
- [prometheus role](https://galaxy.ansible.com/prometheus/prometheus) (optional: if you want to use ansible role prometheus, blackbox-exporter, alert-manager, etc)
  ```bash
  ansible-galaxy collection install prometheus.prometheus
  ```
- [patrickjahns promtail role](https://github.com/patrickjahns/ansible-role-promtail) (optional: if you want to use promtail ansible role by patrickjahns)
  ```bash
  ansible-galaxy install patrickjahns.promtail
  ```

## Run the script
clone the project to your favourite location, cd into the cloned project, and running by do this command:
```bash
bash setup-server.sh
```
or make it executable first:
```bash
chmod +x setup-server.sh
```
and then execute the script:
```bash
./setup-server.sh
```

## Troubleshooting
### Fork break MacOS
```
objc[41011]: +[__NSCFConstantString initialize] may have been in progress in another thread when fork() was called. We cannot safely call it or ignore it in the fork() child process. Crashing instead. Set a breakpoint on objc_initializeAfterForkError to debug.
ERROR! A worker was found in a dead state 
```
##### Resolution:
- current session only
```bash
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES
```
- for all future sessions
```bash
echo "OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES" >> .bash_profile
```
Save, exit, close terminal, and re-open the terminal. Verify with:
```bash
env
```
example output:
```bash
[...]
OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES
```
### prometheus role
#### GNU tar required MacOS
```
fatal: [43.218.99.7 -> localhost]: FAILED! => {"changed": false, "msg": "Failed to find handler for \"/Users/ug/.ansible/tmp/ansible-tmp-1689805926.022539-37335-21231013104032/source\". Make sure the required command to extract the file is installed.\nCommand \"/usr/bin/tar\" detected as tar type bsd. GNU tar required.\nCommand \"/usr/bin/unzip\" could not handle archive:   End-of-central-directory signature not found.  Either this file is not\n  a zipfile, or it constitutes one disk of a multi-part archive.  In the\n  latter case the central directory and zipfile comment will be found on\n  the last disk(s) of this archive.\nnote:  /Users/ug/.ansible/tmp/ansible-tmp-1689805926.022539-37335-21231013104032/source may be a plain executable, not an archive\nunzip:  cannot find zipfile directory in one of /Users/ug/.ansible/tmp/ansible-tmp-1689805926.022539-37335-21231013104032/source or\n        /Users/ug/.ansible/tmp/ansible-tmp-1689805926.022539-37335-21231013104032/source.zip, and cannot find /Users/ug/.ansible/tmp/ansible-tmp-1689805926.022539-37335-21231013104032/source.ZIP, period.\n"}
```
##### Resolution:
```bash
brew install gnu-tar
```
