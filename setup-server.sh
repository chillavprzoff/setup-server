#!/bin/bash

OS=($(ls))

NAME_HOSTS="servers"

if [[ ! -d tmp ]]; then
	mkdir tmp
fi

if [[ -f tmp/ip$NAME_HOSTS ]]; then
	rm -rf tmp/ip$NAME_HOSTS
	yq e '.servers.hosts' inventory.yaml | sed "s/://g" | sed "s/null//g" | sed "s/ //g" >./tmp/ip${NAME_HOSTS}
fi

if [[ -f .tmpenv ]]; then
	rm -rf .tmpenv
fi

for o in ${OS[@]}; do
	if [[ -d $o ]] && [[ $o != "tmp" ]] && [[ $o != "scripts" ]] && [[ $o != "files" ]]; then
		LIST_OS+=("$o")
	fi
done

INVENTORY_FILE="inventory.yaml"

if [[ -f $INVENTORY_FILE ]]; then
	echo "found inventory file!"
	echo
	cat $INVENTORY_FILE
	echo
	read -p "Use the inventory file? (y/N): " USE_EXISTING_INVENTORY_FILE
	if [[ $USE_EXISTING_INVENTORY_FILE != "y" ]] && [[ $USE_EXISTING_INVENTORY_FILE != "Y" ]]; then
		rm -rf $INVENTORY_FILE
	else
		yq e '.servers.hosts' inventory.yaml | sed "s/://g" | sed "s/null//g" | sed "s/ //g" >./tmp/ip${NAME_HOSTS}-uniq
	fi
fi

export INVENTORY_FILE_EXAMPLE="../inventory.yaml-example"
export INVENTORY_FILE="../$INVENTORY_FILE"
export SETUP_SCRIPT_ENABLED=true

while [[ true ]]; do

	PS3='Please choose OS: '
	select opt in "${LIST_OS[@]}"; do
		if [[ ! -d $opt/scripts ]]; then
			ln -s $(pwd)/scripts $(pwd)/$opt/scripts
		fi
		if [[ ! -d $opt/tmp ]]; then
			ln -s $(pwd)/tmp $(pwd)/$opt/tmp
		fi
		if [[ ! -d $opt/files ]]; then
			ln -s $(pwd)/files $(pwd)/$opt/files
		fi
		bash ./$opt/setup-server.sh
		break
	done

done
