import getpass
import bcrypt
import sys

if len(sys.argv) > 1:
    if sys.argv[1]:
        hashed_password = bcrypt.hashpw(sys.argv[1].encode("utf-8"), bcrypt.gensalt())
else:
    password = getpass.getpass("password: ")
    hashed_password = bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt())
print(hashed_password.decode())
